<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/view_post.html.twig */
class __TwigTemplate_52e04efbfd89f433252d3da7887d49a5ce36ce16daa8f8b2a8163d7abc463b6d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/view_post.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/view_post.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "post/view_post.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 4, $this->source); })()), "title", [], "any", false, false, false, 4), "html", null, true);
        echo " - Lebonartisan.ma
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "\t<div class=\"ftco-candidates ftco-candidates-2 bg-light mt-5\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5 mt-5\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<div class=\"p-5 bg-white rounded\">
\t\t\t\t\t\t\t<div id=\"carouselExampleIndicators\" class=\"carousel slide rounded\" data-ride=\"carousel\">
\t\t\t\t\t\t\t\t<ol class=\"carousel-indicators\">
\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
\t\t\t\t\t\t\t\t\t";
        // line 17
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 17, $this->source); })()), "photo2", [], "any", false, false, false, 17))) {
            // line 18
            echo "\t\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
\t\t\t\t\t\t\t\t\t";
        }
        // line 20
        echo "\t\t\t\t\t\t\t\t\t";
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 20, $this->source); })()), "photo3", [], "any", false, false, false, 20))) {
            // line 21
            echo "\t\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
\t\t\t\t\t\t\t\t\t";
        }
        // line 23
        echo "\t\t\t\t\t\t\t\t\t";
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 23, $this->source); })()), "photo4", [], "any", false, false, false, 23))) {
            // line 24
            echo "\t\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"3\"></li>
\t\t\t\t\t\t\t\t\t";
        }
        // line 26
        echo "\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t\t\t\t\t";
        // line 28
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 28, $this->source); })()), "photo1", [], "any", false, false, false, 28))) {
            // line 29
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item active w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/post_photos/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 30, $this->source); })()), "photo1", [], "any", false, false, false, 30), "html", null, true);
            echo "\" alt=\"First slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        } else {
            // line 33
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/GALLERY-512.png"), "html", null, true);
            echo "\" alt=\"First slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        // line 37
        echo "\t\t\t\t\t\t\t\t\t";
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 37, $this->source); })()), "photo2", [], "any", false, false, false, 37))) {
            // line 38
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/post_photos/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 39, $this->source); })()), "photo2", [], "any", false, false, false, 39), "html", null, true);
            echo "\" alt=\"Second slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        // line 42
        echo "\t\t\t\t\t\t\t\t\t";
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 42, $this->source); })()), "photo3", [], "any", false, false, false, 42))) {
            // line 43
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/post_photos/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 44, $this->source); })()), "photo3", [], "any", false, false, false, 44), "html", null, true);
            echo "\" alt=\"Third slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        // line 47
        echo "\t\t\t\t\t\t\t\t\t";
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 47, $this->source); })()), "photo4", [], "any", false, false, false, 47))) {
            // line 48
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/post_photos/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 49, $this->source); })()), "photo4", [], "any", false, false, false, 49), "html", null, true);
            echo "\" alt=\"Fourth slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        // line 52
        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
\t\t\t\t\t\t\t\t\t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">Previous</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
\t\t\t\t\t\t\t\t\t<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">Next</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row d-flex justify-content-left ml-4 mt-3\">
\t\t\t\t\t\t\t\t<h2>";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 63, $this->source); })()), "title", [], "any", false, false, false, 63), "html", null, true);
        echo "</h2>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<span class=\"seen ml-4 mr-3\">
\t\t\t\t\t\t\t\t";
        // line 66
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 66, $this->source); })()), "createdOn", [], "any", false, false, false, 66), "d/m/Y à H:i"), "html", null, true);
        echo "</span>
\t\t\t\t\t\t\t<span class=\"icon-my_location\"></span>
\t\t\t\t\t\t\t<span class=\"mr-3\">";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 68, $this->source); })()), "city", [], "any", false, false, false, 68), "label", [], "any", false, false, false, 68), "html", null, true);
        echo "</span>
\t\t\t\t\t\t\t<span class=\"position\">";
        // line 69
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 69, $this->source); })()), "category", [], "any", false, false, false, 69), "label", [], "any", false, false, false, 69), "html", null, true);
        echo "</span>
\t\t\t\t\t\t\t<hr class=\"mb-5\">
\t\t\t\t\t\t\t<h2 class=\"mb-3\">Description</h2>
\t\t\t\t\t\t\t<p>";
        // line 72
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 72, $this->source); })()), "description", [], "any", false, false, false, 72), "html", null, true);
        echo "</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-4 mt-5\">
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"p-4 mb-3 bg-white pers-infos\">
\t\t\t\t\t\t";
        // line 81
        echo "
\t\t\t\t\t\t<p class=\"mb-4 d-flex justify-content-center\">
\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t<i class=\"icon-id-card-o mr-2 mt-1 mb-1\"></i>
\t\t\t\t\t\t\t\t";
        // line 85
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 85, $this->source); })()), "firstname", [], "any", false, false, false, 85), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t";
        // line 86
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 86, $this->source); })()), "lastname", [], "any", false, false, false, 86), "html", null, true);
        echo "</strong>
\t\t\t\t\t\t</p>

\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold d-flex justify-content-center bg-dark rounded mb-1\">
\t\t\t\t\t\t\t<i class=\"icon-phone mr-2 mt-1 mb-1\"></i>Téléphone
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"mb-4 d-flex justify-content-center\">
\t\t\t\t\t\t\t<a href=\"#\">(+212)
\t\t\t\t\t\t\t\t";
        // line 94
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 94, $this->source); })()), "phone", [], "any", false, false, false, 94), "html", null, true);
        echo "</a>
\t\t\t\t\t\t</p>

\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold d-flex justify-content-center bg-dark rounded mb-1\">
\t\t\t\t\t\t\t<i class=\"icon-envelope mr-2 mt-1 mb-1\"></i>
\t\t\t\t\t\t\tAdresse e-mail</p>
\t\t\t\t\t\t<p class=\"mb-0 d-flex justify-content-center\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t<span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">";
        // line 102
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 102, $this->source); })()), "email", [], "any", false, false, false, 102), "html", null, true);
        echo "</span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</p>

\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"p-4 mb-3 bg-white d-flex d-flex justify-content-center\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t</img>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "post/view_post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 102,  253 => 94,  242 => 86,  238 => 85,  232 => 81,  221 => 72,  215 => 69,  211 => 68,  206 => 66,  200 => 63,  187 => 52,  180 => 49,  177 => 48,  174 => 47,  167 => 44,  164 => 43,  161 => 42,  154 => 39,  151 => 38,  148 => 37,  142 => 34,  139 => 33,  132 => 30,  129 => 29,  127 => 28,  123 => 26,  119 => 24,  116 => 23,  112 => 21,  109 => 20,  105 => 18,  103 => 17,  92 => 8,  82 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\t{{post.title}} - Lebonartisan.ma
{% endblock %}

{% block body %}
\t<div class=\"ftco-candidates ftco-candidates-2 bg-light mt-5\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5 mt-5\">
\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t<div class=\"p-5 bg-white rounded\">
\t\t\t\t\t\t\t<div id=\"carouselExampleIndicators\" class=\"carousel slide rounded\" data-ride=\"carousel\">
\t\t\t\t\t\t\t\t<ol class=\"carousel-indicators\">
\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>
\t\t\t\t\t\t\t\t\t{% if post.photo2 is not null %}
\t\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% if post.photo3 is not null %}
\t\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% if post.photo4 is not null %}
\t\t\t\t\t\t\t\t\t\t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"3\"></li>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t\t\t\t\t{% if post.photo1 is not null %}
\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item active w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"{{asset('uploads/post_photos/')}}{{post.photo1}}\" alt=\"First slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"{{asset('images/GALLERY-512.png')}}\" alt=\"First slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% if post.photo2 is not null %}
\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"{{asset('uploads/post_photos/')}}{{post.photo2}}\" alt=\"Second slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% if post.photo3 is not null %}
\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"{{asset('uploads/post_photos/')}}{{post.photo3}}\" alt=\"Third slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% if post.photo4 is not null %}
\t\t\t\t\t\t\t\t\t\t<div class=\"carousel-item w-100 h-100\">
\t\t\t\t\t\t\t\t\t\t\t<img style=\"display: block;height: 400px;\" class=\"img-responsive d-block w-100\" src=\"{{asset('uploads/post_photos/')}}{{post.photo4}}\" alt=\"Fourth slide\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
\t\t\t\t\t\t\t\t\t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">Previous</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
\t\t\t\t\t\t\t\t\t<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">Next</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row d-flex justify-content-left ml-4 mt-3\">
\t\t\t\t\t\t\t\t<h2>{{post.title}}</h2>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<span class=\"seen ml-4 mr-3\">
\t\t\t\t\t\t\t\t{{ post.createdOn |date(\"d/m/Y à H:i\")}}</span>
\t\t\t\t\t\t\t<span class=\"icon-my_location\"></span>
\t\t\t\t\t\t\t<span class=\"mr-3\">{{ post.city.label }}</span>
\t\t\t\t\t\t\t<span class=\"position\">{{ post.category.label}}</span>
\t\t\t\t\t\t\t<hr class=\"mb-5\">
\t\t\t\t\t\t\t<h2 class=\"mb-3\">Description</h2>
\t\t\t\t\t\t\t<p>{{ post.description}}</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-4 mt-5\">
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"p-4 mb-3 bg-white pers-infos\">
\t\t\t\t\t\t{# <h3 class=\"h5 text-black mb-3 d-flex justify-content-center\">Informations personnelles</h3> #}

\t\t\t\t\t\t<p class=\"mb-4 d-flex justify-content-center\">
\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t<i class=\"icon-id-card-o mr-2 mt-1 mb-1\"></i>
\t\t\t\t\t\t\t\t{{post.firstname}}
\t\t\t\t\t\t\t\t{{post.lastname}}</strong>
\t\t\t\t\t\t</p>

\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold d-flex justify-content-center bg-dark rounded mb-1\">
\t\t\t\t\t\t\t<i class=\"icon-phone mr-2 mt-1 mb-1\"></i>Téléphone
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"mb-4 d-flex justify-content-center\">
\t\t\t\t\t\t\t<a href=\"#\">(+212)
\t\t\t\t\t\t\t\t{{post.phone}}</a>
\t\t\t\t\t\t</p>

\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold d-flex justify-content-center bg-dark rounded mb-1\">
\t\t\t\t\t\t\t<i class=\"icon-envelope mr-2 mt-1 mb-1\"></i>
\t\t\t\t\t\t\tAdresse e-mail</p>
\t\t\t\t\t\t<p class=\"mb-0 d-flex justify-content-center\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t<span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">{{post.email}}</span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</p>

\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"p-4 mb-3 bg-white d-flex d-flex justify-content-center\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t</img>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>{% endblock %}
", "post/view_post.html.twig", "/Users/marouane/lebonartisan/templates/post/view_post.html.twig");
    }
}
