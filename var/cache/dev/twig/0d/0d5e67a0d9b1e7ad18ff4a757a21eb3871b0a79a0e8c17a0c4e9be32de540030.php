<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* artisan_auth/index.html.twig */
class __TwigTemplate_6c40a3383560399c2bf2f7dab039d959b839c5c3f04c0c18fd1317a6be25986a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "artisan_auth/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "artisan_auth/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "artisan_auth/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Espace Artisan : Identifiez-vous";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"ftco-section bg-light\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12 col-lg-8 mb-5\">
                <form class=\"p-5 bg-white\" method=\"post\" action=";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("artisan_auth");
        echo " >
                    <h3 class=\"mb-5\">Identifiez-vous</h3>
                    <div class=\"row form-group\">
                        <div class=\"col-md-12 mb-3 mb-md-0\">
                            <label class=\"font-weight-bold\" for=\"fullname\">Adresse e-mail</label>
                            <input type=\"text\" class=\"form-control py-1\" required name=\"_username\">
                        </div>
                    </div>
                    <div class=\"row form-group\">
                        <div class=\"col-md-12 mb-3 mb-md-0\">
                            <label class=\"font-weight-bold\" for=\"fullname\">Mot de passe</label>
                            <input type=\"password\" class=\"form-control py-1\" required name=\"_password\"/>
                        </div>
                    </div>
                    <div class=\"d-flex justify-content-between mb-5\">
                        <a class=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("artisan_auth");
        echo "\" href=\"\">Mot de passe oublié ?</a>
                        <span/>
                        <a href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_registration");
        echo "\">Créer un compte</a>
                    </div>
                    <div class=\"d-flex justify-content-center mb-3\">
                        <button type=\"submit\" class=\"btn btn-primary  py-2 px-4 w-25\" value=\"Connexion\">Connexion</button>
                    </div>
                    </form>
                    
                </div>

                <div class=\"col-lg-4\">
                    <div class=\"p-4 mb-3 bg-white\">
                        <h3 class=\"h5 text-black mb-3\">Contact Info</h3>
                        <p class=\"mb-0 font-weight-bold\">Address</p>
                        <p class=\"mb-4\">203 Fake St. Mountain View, San Francisco, California, USA</p>

                        <p class=\"mb-0 font-weight-bold\">Phone</p>
                        <p class=\"mb-4\">
                            <a href=\"#\">+1 232 3235 324</a>
                        </p>

                        <p class=\"mb-0 font-weight-bold\">Email Address</p>
                        <p class=\"mb-0\">
                            <a href=\"#\">
                                <span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">[email&#160;protected]</span>
                            </a>
                        </p>

                    </div>

                    <div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t\t</img>
\t\t\t\t\t\t</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "artisan_auth/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 27,  112 => 25,  94 => 10,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Espace Artisan : Identifiez-vous{% endblock %}

{% block body %}
    <div class=\"ftco-section bg-light\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12 col-lg-8 mb-5\">
                <form class=\"p-5 bg-white\" method=\"post\" action={{path('artisan_auth')}} >
                    <h3 class=\"mb-5\">Identifiez-vous</h3>
                    <div class=\"row form-group\">
                        <div class=\"col-md-12 mb-3 mb-md-0\">
                            <label class=\"font-weight-bold\" for=\"fullname\">Adresse e-mail</label>
                            <input type=\"text\" class=\"form-control py-1\" required name=\"_username\">
                        </div>
                    </div>
                    <div class=\"row form-group\">
                        <div class=\"col-md-12 mb-3 mb-md-0\">
                            <label class=\"font-weight-bold\" for=\"fullname\">Mot de passe</label>
                            <input type=\"password\" class=\"form-control py-1\" required name=\"_password\"/>
                        </div>
                    </div>
                    <div class=\"d-flex justify-content-between mb-5\">
                        <a class=\"{{path('artisan_auth')}}\" href=\"\">Mot de passe oublié ?</a>
                        <span/>
                        <a href=\"{{path('security_registration')}}\">Créer un compte</a>
                    </div>
                    <div class=\"d-flex justify-content-center mb-3\">
                        <button type=\"submit\" class=\"btn btn-primary  py-2 px-4 w-25\" value=\"Connexion\">Connexion</button>
                    </div>
                    </form>
                    
                </div>

                <div class=\"col-lg-4\">
                    <div class=\"p-4 mb-3 bg-white\">
                        <h3 class=\"h5 text-black mb-3\">Contact Info</h3>
                        <p class=\"mb-0 font-weight-bold\">Address</p>
                        <p class=\"mb-4\">203 Fake St. Mountain View, San Francisco, California, USA</p>

                        <p class=\"mb-0 font-weight-bold\">Phone</p>
                        <p class=\"mb-4\">
                            <a href=\"#\">+1 232 3235 324</a>
                        </p>

                        <p class=\"mb-0 font-weight-bold\">Email Address</p>
                        <p class=\"mb-0\">
                            <a href=\"#\">
                                <span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">[email&#160;protected]</span>
                            </a>
                        </p>

                    </div>

                    <div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t\t</img>
\t\t\t\t\t\t</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
", "artisan_auth/index.html.twig", "/Users/marouane/lebonartisan/templates/artisan_auth/index.html.twig");
    }
}
