<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* profile/index.html.twig */
class __TwigTemplate_fd344c99dc73d2c8d8e302f4482fcb64af0ced66beee3f24d8483353c6543257 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "profile/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "profile/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "profile/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Le Bon Artisan
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
\t<section class=\"ftco-section bg-light\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">


\t\t\t\t<div class=\"col-lg-4\">
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"p-4 mb-3 bg-white pers-infos\">
\t\t\t\t\t\t";
        // line 17
        echo "\t\t\t\t\t\t<div class=\"d-flex justify-content-center\">
\t\t\t\t\t\t\t";
        // line 18
        if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 18, $this->source); })()), "photo", [], "any", false, false, false, 18)) ||  !twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 18, $this->source); })()), "photo", [], "any", false, false, false, 18)))) {
            // line 19
            echo "\t\t\t\t\t\t\t\t<div class=\"user-img mb-4 mt-1\" style=\"background-image: url(";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/profile_photos/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 19, $this->source); })()), "photo", [], "any", false, false, false, 19), "html", null, true);
            echo ");\">
\t\t\t\t\t\t\t\t";
        } else {
            // line 21
            echo "\t\t\t\t\t\t\t\t\t";
            if ((twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 21, $this->source); })()), "sexe", [], "any", false, false, false, 21) == 0)) {
                // line 22
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"user-img mb-4 mt-1\" style=\"background-image: url(";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/profile_photos/man.png"), "html", null, true);
                echo ");\">
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 24
                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-img mb-4 mt-1\" style=\"background-image: url(";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/profile_photos/woman.png"), "html", null, true);
                echo ");\">
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 26
            echo "\t\t\t\t\t\t\t\t\t\t";
        }
        // line 27
        echo "\t\t\t\t\t\t\t\t\t\t";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY") && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 27, $this->source); })()), "user", [], "any", false, false, false, 27), "id", [], "any", false, false, false, 27) == twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 27, $this->source); })()), "id", [], "any", false, false, false, 27)))) {
            // line 28
            echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"quote d-flex align-items-center justify-content-center\" id=\"amodal\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-toggle=\"modal\" data-target=\"#bannerformmodal\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-camera\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
        }
        // line 34
        echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p class=\"mb-0 d-flex justify-content-center\">
\t\t\t\t\t\t\t\t\t<strong>";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 37, $this->source); })()), "firstname", [], "any", false, false, false, 37), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t\t\t";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 38, $this->source); })()), "lastname", [], "any", false, false, false, 38), "html", null, true);
        echo "</strong>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p class=\"mb-4 d-flex justify-content-center\">";
        // line 40
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 40, $this->source); })()), "category", [], "any", false, false, false, 40), "label", [], "any", false, false, false, 40), "html", null, true);
        echo "</p>

\t\t\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">
\t\t\t\t\t\t\t\t\t<i class=\"icon-map-marker\"></i>
\t\t\t\t\t\t\t\t\tLocalisation</p>
\t\t\t\t\t\t\t\t<p class=\"mb-4\">";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 45, $this->source); })()), "quartier", [], "any", false, false, false, 45), "html", null, true);
        echo ",
\t\t\t\t\t\t\t\t\t";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 46, $this->source); })()), "city", [], "any", false, false, false, 46), "label", [], "any", false, false, false, 46), "html", null, true);
        echo "</p>

\t\t\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">
\t\t\t\t\t\t\t\t\t<i class=\"icon-phone\"></i>
\t\t\t\t\t\t\t\t\tTéléphone</p>
\t\t\t\t\t\t\t\t<p class=\"mb-4\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">(+212)
\t\t\t\t\t\t\t\t\t\t";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 53, $this->source); })()), "phoneNumber", [], "any", false, false, false, 53), "html", null, true);
        echo "</a>
\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">
\t\t\t\t\t\t\t\t\t<i class=\"icon-envelope\"></i>
\t\t\t\t\t\t\t\t\tAdresse e-mail</p>
\t\t\t\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t<span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">";
        // line 61
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 61, $this->source); })()), "email", [], "any", false, false, false, 61), "html", null, true);
        echo "</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"p-4 mb-3\">
\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t\t\t</img>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-md-12 col-lg-8\">

\t\t\t\t\t\t<div class=\"p-5 bg-white\">
\t\t\t\t\t\t\t<h3 class=\"mb-2\">Informations personnelles</h3>
\t\t\t\t\t\t\t<hr class=\"mb-5\">

\t\t\t\t\t\t\t<div class=\"row mb-0\">
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-venus-mars\"></i>
\t\t\t\t\t\t\t\t\t\t\tSexe</strong>
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t";
        // line 88
        if ((twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 88, $this->source); })()), "sexe", [], "any", false, false, false, 88) == 0)) {
            echo "Homme";
        } else {
            echo "Femme
\t\t\t\t\t\t\t\t\t\t\t";
        }
        // line 90
        echo "\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-dollar\"></i>
\t\t\t\t\t\t\t\t\t\t\tCoût horaire (Dhs)</strong>
\t\t\t\t\t\t\t\t\t\t<p>";
        // line 98
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 98, $this->source); })()), "hourlyCost", [], "any", false, false, false, 98), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"row mb-5\">
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-clock-o\"></i>
\t\t\t\t\t\t\t\t\t\t\tHeures de travail</strong>
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t";
        // line 110
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 110, $this->source); })()), "heureDebut", [], "any", false, false, false, 110), "html", null, true);
        echo "h -
\t\t\t\t\t\t\t\t\t\t\t";
        // line 111
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 111, $this->source); })()), "heureFin", [], "any", false, false, false, 111), "html", null, true);
        echo "h
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-calendar-o\"></i>
\t\t\t\t\t\t\t\t\t\t\tJours disponibles</strong>
\t\t\t\t\t\t\t\t\t\t<p>";
        // line 120
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 120, $this->source); })()), "hourlyCost", [], "any", false, false, false, 120), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t<i class=\"icon-pencil-square-o\"></i>
\t\t\t\t\t\t\t\t\tDescription de l'activité</strong>
\t\t\t\t\t\t\t\t<p>";
        // line 129
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 129, $this->source); })()), "description", [], "any", false, false, false, 129), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t";
        // line 132
        if ((((isset($context["form"]) || array_key_exists("form", $context)) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 132, $this->source); })()), "user", [], "any", false, false, false, 132), "id", [], "any", false, false, false, 132) == twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 132, $this->source); })()), "id", [], "any", false, false, false, 132)))) {
            // line 133
            echo "\t\t\t\t\t\t\t\t<div class=\"d-flex justify-content-center mt-5\">
\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"btn btn-primary  py-2 px-4\">Mette à jour le profil</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        // line 137
        echo "\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"container mt-3\">
\t\t\t\t\t\t\t<h3 class=\"text-center\">Avis des clients</h3>

\t\t\t\t\t\t\t<div class=\"card\" id=\"cards\">
\t\t\t\t\t\t\t\t";
        // line 144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 144, $this->source); })()), "comments", [], "any", false, false, false, 144));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 145
            echo "\t\t\t\t\t\t\t\t\t";
            if ((null === twig_get_attribute($this->env, $this->source, $context["comment"], "replyTo", [], "any", false, false, false, 145))) {
                // line 146
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"https://image.ibb.co/jw55Ex/def_face.jpg\" class=\"img img-rounded img-fluid\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://maniruzzaman-akash.blogspot.com/p/contact.html\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong>";
                // line 154
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "commentator", [], "any", false, false, false, 154), "html", null, true);
                echo "</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right mr-3 mt-1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">";
                // line 157
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "likes", [], "any", false, false, false, 157), "html", null, true);
                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-up\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right ml-3 mr-3 mt-1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">";
                // line 163
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "dislikes", [], "any", false, false, false, 163), "html", null, true);
                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-down\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-secondary mb-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"seen\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>";
                // line 169
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "createdOn", [], "any", false, false, false, 169), "d/m/Y à H:m"), "html", null, true);
                echo "</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">";
                // line 172
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "text", [], "any", false, false, false, 172), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                // line 176
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["comment"], "comments", [], "any", false, false, false, 176));
                foreach ($context['_seq'] as $context["_key"] => $context["subcomment"]) {
                    // line 177
                    echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"card card-inner mt-0 mr-3 ml-4\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"https://image.ibb.co/jw55Ex/def_face.jpg\" class=\"img img-rounded img-fluid\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://maniruzzaman-akash.blogspot.com/p/contact.html\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong>";
                    // line 186
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subcomment"], "commentator", [], "any", false, false, false, 186), "html", null, true);
                    echo "</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right mr-3 mt-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">";
                    // line 189
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subcomment"], "likes", [], "any", false, false, false, 189), "html", null, true);
                    echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-up\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right ml-3 mr-3 mt-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">";
                    // line 195
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subcomment"], "dislikes", [], "any", false, false, false, 195), "html", null, true);
                    echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-down\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-secondary mb-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"seen\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>";
                    // line 201
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subcomment"], "createdOn", [], "any", false, false, false, 201), "d/m/Y à H:m"), "html", null, true);
                    echo "</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">";
                    // line 204
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subcomment"], "text", [], "any", false, false, false, 204), "html", null, true);
                    echo "</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcomment'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 210
                echo "\t\t\t\t\t\t\t\t\t\t";
                if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY") && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 210, $this->source); })()), "user", [], "any", false, false, false, 210), "id", [], "any", false, false, false, 210) == twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 210, $this->source); })()), "id", [], "any", false, false, false, 210)))) {
                    // line 211
                    echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"mt-2 mr-3 ml-4\">
\t\t\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" placeholder=\"Commentez en retour ...\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"float-right btn btn-outline-primary ml-2 mb-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-reply\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tRépondre</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 224
                echo "\t\t\t\t\t\t\t\t\t";
            }
            // line 225
            echo "\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 226
        echo "
\t\t\t\t\t\t\t\t";
        // line 227
        if ( !$this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 228
            echo "\t\t\t\t\t\t\t\t\t<form method=\"post\" action=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("submit_comment");
            echo "\">
\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"h5 text-black\">Donnez votre avis</h5>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"card card-inner mt-1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"https://image.ibb.co/jw55Ex/def_face.jpg\" class=\"img img-rounded img-fluid\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"commentator\" required type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nom Complet\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"artisanId\" name=\"id\" type=\"hidden\" value=\"";
            // line 241
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["artisan"]) || array_key_exists("artisan", $context) ? $context["artisan"] : (function () { throw new RuntimeError('Variable "artisan" does not exist.', 241, $this->source); })()), "id", [], "any", false, false, false, 241), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"comment\" required class=\"form-control\" name=\"comment\" placeholder=\"Commentaire ...\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"float-right btn btn-outline-primary ml-2\" id=\"commentbutton\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-reply\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tCommenter
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Modal -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-dialog\" role=\"document\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-check mr-3\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tVotre commentaire a été enregistré.</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary float-right\" data-dismiss=\"modal\">Fermer</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal fade bannerformmodal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"bannerformmodal\" aria-hidden=\"true\" id=\"bannerformmodal\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-dialog\" role=\"document\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"modal-title\">Modal title</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Modal body text goes here.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t";
        }
        // line 290
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>
\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 297
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        // line 298
        echo "\t\t";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "
\t\t<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\" integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js\" integrity=\"sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\"></script>
\t\t<script>
\t\t\tvar p = document.getElementById(\"amodal\");
\t\t\tp.onclick = function() { \$(\"#exampleModal\").modal();}; 
\t\t\t\$(document).on('submit', 'form', function (e) { // il est impératif de commencer avec cette méthode qui va empêcher le navigateur d'envoyer le formulaire lui-même
\t\t\te.preventDefault();
\t\t\t\$form = \$(e.target);
\t\t\tdebugger;
\t\t\tvar comment = document.getElementById('comment').value;
\t\t\tvar commentator = document.getElementById('commentator').value;

\t\t\tif (comment == '' || commentator == '') {
\t\t\treturn;
\t\t\t}

\t\t\t\$.ajax({type: \$(this).attr('method'), url: \$(this).attr('action'), data: \$(this).serialize()}).done(function (data) {
\t\t\tif (typeof data.commentator !== 'undefined') { // alert(data.message);
\t\t\tdocument.getElementById('comment').value = '';
\t\t\tdocument.getElementById('commentator').value = '';
\t\t\t\$(\"#exampleModal\").modal();
\t\t\t// document.getElementById(\"myDialog\").showModal();
\t\t\t// alert(data.text);
\t\t\tvar html = \"<div class='card-body'>\" + \"<div class='row'>\" + \"<div class='col-md-2'>\" + \"<img src='https://image.ibb.co/jw55Ex/def_face.jpg' class='img img-rounded img-fluid'/>\" + \"</div>\" + \"<div class='col-md-10'>\" + \"<p class='mb-0'>\" + \"<a href='https://maniruzzaman-akash.blogspot.com/p/contact.html'>\" + \"<strong>\" + data.commentator + \"</strong>\" + \"</a>\" + \"<span class='float-right mr-3 mt-1'>\" + \"<span class='number'>\" + data.likes + \" </span>\" + \"<a href=''>\" + \"<i class='icon-thumbs-o-up'></i>\" + \"</a>\" + \"</span>\" + \"<span class='float-right ml-3 mr-3 mt-1'>\" + \"<span class='number'>\" + data.dislikes + \" </span>\" + \"<i class='icon-thumbs-o-down'></i>\" + \"</span>\" + \"</p>\" + \"<p class='text-secondary mb-2'>\" + \"<span class='seen'>\" + \"<small>à l'instant</small>\" + \"</span>\" + \"</p>\" + \"<p class='mb-0'>\" + data.text + \"</p>\";
\t\t\t\$(\"#cards\").prepend(html);
\t\t\t}
\t\t\t}).fail(function (jqXHR, textStatus, errorThrown) {
\t\t\tif (typeof jqXHR.responseJSON !== 'undefined') {
\t\t\tif (jqXHR.responseJSON.hasOwnProperty('form')) { // \$('#form_body').html(jqXHR.responseJSON.form);
\t\t\t}
\t\t\t// \$('.form_error').html(jqXHR.responseJSON.message);

\t\t\t} else {
\t\t\talert(errorThrown);
\t\t\t}

\t\t\t});

\t\t\t});
</script>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "profile/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  540 => 298,  530 => 297,  514 => 290,  462 => 241,  445 => 228,  443 => 227,  440 => 226,  434 => 225,  431 => 224,  416 => 211,  413 => 210,  401 => 204,  395 => 201,  386 => 195,  377 => 189,  371 => 186,  360 => 177,  356 => 176,  349 => 172,  343 => 169,  334 => 163,  325 => 157,  319 => 154,  309 => 146,  306 => 145,  302 => 144,  293 => 137,  287 => 133,  285 => 132,  279 => 129,  267 => 120,  255 => 111,  251 => 110,  236 => 98,  226 => 90,  219 => 88,  189 => 61,  178 => 53,  168 => 46,  164 => 45,  156 => 40,  151 => 38,  147 => 37,  142 => 34,  134 => 28,  131 => 27,  128 => 26,  122 => 24,  116 => 22,  113 => 21,  106 => 19,  104 => 18,  101 => 17,  90 => 7,  80 => 6,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Le Bon Artisan
{% endblock %}

{% block body %}

\t<section class=\"ftco-section bg-light\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">


\t\t\t\t<div class=\"col-lg-4\">
\t\t\t\t\t<div
\t\t\t\t\t\tclass=\"p-4 mb-3 bg-white pers-infos\">
\t\t\t\t\t\t{# <h3 class=\"h5 text-black mb-3 d-flex justify-content-center\">Informations personnelles</h3> #}
\t\t\t\t\t\t<div class=\"d-flex justify-content-center\">
\t\t\t\t\t\t\t{% if artisan.photo is not null or artisan.photo is not empty %}
\t\t\t\t\t\t\t\t<div class=\"user-img mb-4 mt-1\" style=\"background-image: url({{asset('uploads/profile_photos/')}}{{artisan.photo}});\">
\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t{% if artisan.sexe == 0 %}
\t\t\t\t\t\t\t\t\t\t<div class=\"user-img mb-4 mt-1\" style=\"background-image: url({{asset('uploads/profile_photos/man.png')}});\">
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t<div class=\"user-img mb-4 mt-1\" style=\"background-image: url({{asset('uploads/profile_photos/woman.png')}});\">
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t{% if is_granted('IS_AUTHENTICATED_FULLY') and app.user.id == artisan.id %}
\t\t\t\t\t\t\t\t\t\t\t<span class=\"quote d-flex align-items-center justify-content-center\" id=\"amodal\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-toggle=\"modal\" data-target=\"#bannerformmodal\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-camera\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<p class=\"mb-0 d-flex justify-content-center\">
\t\t\t\t\t\t\t\t\t<strong>{{artisan.firstname}}
\t\t\t\t\t\t\t\t\t\t{{artisan.lastname}}</strong>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p class=\"mb-4 d-flex justify-content-center\">{{artisan.category.label}}</p>

\t\t\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">
\t\t\t\t\t\t\t\t\t<i class=\"icon-map-marker\"></i>
\t\t\t\t\t\t\t\t\tLocalisation</p>
\t\t\t\t\t\t\t\t<p class=\"mb-4\">{{artisan.quartier}},
\t\t\t\t\t\t\t\t\t{{artisan.city.label}}</p>

\t\t\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">
\t\t\t\t\t\t\t\t\t<i class=\"icon-phone\"></i>
\t\t\t\t\t\t\t\t\tTéléphone</p>
\t\t\t\t\t\t\t\t<p class=\"mb-4\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">(+212)
\t\t\t\t\t\t\t\t\t\t{{artisan.phoneNumber}}</a>
\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">
\t\t\t\t\t\t\t\t\t<i class=\"icon-envelope\"></i>
\t\t\t\t\t\t\t\t\tAdresse e-mail</p>
\t\t\t\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t<span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">{{artisan.email}}</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"p-4 mb-3\">
\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t\t\t</img>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-md-12 col-lg-8\">

\t\t\t\t\t\t<div class=\"p-5 bg-white\">
\t\t\t\t\t\t\t<h3 class=\"mb-2\">Informations personnelles</h3>
\t\t\t\t\t\t\t<hr class=\"mb-5\">

\t\t\t\t\t\t\t<div class=\"row mb-0\">
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-venus-mars\"></i>
\t\t\t\t\t\t\t\t\t\t\tSexe</strong>
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t{% if artisan.sexe == 0 %}Homme{% else %}Femme
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-dollar\"></i>
\t\t\t\t\t\t\t\t\t\t\tCoût horaire (Dhs)</strong>
\t\t\t\t\t\t\t\t\t\t<p>{{artisan.hourlyCost}}</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"row mb-5\">
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-clock-o\"></i>
\t\t\t\t\t\t\t\t\t\t\tHeures de travail</strong>
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t{{artisan.heureDebut}}h -
\t\t\t\t\t\t\t\t\t\t\t{{artisan.heureFin}}h
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-calendar-o\"></i>
\t\t\t\t\t\t\t\t\t\t\tJours disponibles</strong>
\t\t\t\t\t\t\t\t\t\t<p>{{artisan.hourlyCost}}</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t<i class=\"icon-pencil-square-o\"></i>
\t\t\t\t\t\t\t\t\tDescription de l'activité</strong>
\t\t\t\t\t\t\t\t<p>{{artisan.description}}</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t{% if form is defined and is_granted('IS_AUTHENTICATED_FULLY') and app.user.id == artisan.id %}
\t\t\t\t\t\t\t\t<div class=\"d-flex justify-content-center mt-5\">
\t\t\t\t\t\t\t\t\t<a href=\"\" class=\"btn btn-primary  py-2 px-4\">Mette à jour le profil</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t</div>


\t\t\t\t\t\t<div class=\"container mt-3\">
\t\t\t\t\t\t\t<h3 class=\"text-center\">Avis des clients</h3>

\t\t\t\t\t\t\t<div class=\"card\" id=\"cards\">
\t\t\t\t\t\t\t\t{% for comment in artisan.comments %}
\t\t\t\t\t\t\t\t\t{% if comment.replyTo is null%}
\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"https://image.ibb.co/jw55Ex/def_face.jpg\" class=\"img img-rounded img-fluid\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://maniruzzaman-akash.blogspot.com/p/contact.html\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong>{{comment.commentator}}</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right mr-3 mt-1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">{{comment.likes}}</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-up\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right ml-3 mr-3 mt-1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">{{comment.dislikes}}</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-down\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-secondary mb-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"seen\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>{{comment.createdOn |date(\"d/m/Y à H:m\")}}</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">{{comment.text}}</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t{% for subcomment in comment.comments %}
\t\t\t\t\t\t\t\t\t\t\t<div class=\"card card-inner mt-0 mr-3 ml-4\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"https://image.ibb.co/jw55Ex/def_face.jpg\" class=\"img img-rounded img-fluid\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://maniruzzaman-akash.blogspot.com/p/contact.html\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong>{{subcomment.commentator}}</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right mr-3 mt-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">{{subcomment.likes}}</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-up\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"float-right ml-3 mr-3 mt-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"number\">{{subcomment.dislikes}}</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-thumbs-o-down\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-secondary mb-0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"seen\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>{{subcomment.createdOn |date(\"d/m/Y à H:m\")}}</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">{{subcomment.text}}</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t{% if is_granted('IS_AUTHENTICATED_FULLY') and app.user.id == artisan.id %}
\t\t\t\t\t\t\t\t\t\t\t<div class=\"mt-2 mr-3 ml-4\">
\t\t\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" placeholder=\"Commentez en retour ...\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"float-right btn btn-outline-primary ml-2 mb-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-reply\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tRépondre</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t{% endfor %}

\t\t\t\t\t\t\t\t{% if not is_granted('IS_AUTHENTICATED_FULLY') %}
\t\t\t\t\t\t\t\t\t<form method=\"post\" action=\"{{path('submit_comment')}}\">
\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"h5 text-black\">Donnez votre avis</h5>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"card card-inner mt-1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"https://image.ibb.co/jw55Ex/def_face.jpg\" class=\"img img-rounded img-fluid\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"commentator\" required type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nom Complet\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"artisanId\" name=\"id\" type=\"hidden\" value=\"{{artisan.id}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"comment\" required class=\"form-control\" name=\"comment\" placeholder=\"Commentaire ...\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"float-right btn btn-outline-primary ml-2\" id=\"commentbutton\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-reply\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tCommenter
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Modal -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-dialog\" role=\"document\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalLabel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-check mr-3\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tVotre commentaire a été enregistré.</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary float-right\" data-dismiss=\"modal\">Fermer</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal fade bannerformmodal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"bannerformmodal\" aria-hidden=\"true\" id=\"bannerformmodal\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-dialog\" role=\"document\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"modal-title\">Modal title</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Modal body text goes here.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>
\t{% endblock %}
\t{% block javascript %}
\t\t{{ parent() }}
\t\t<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\" integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js\" integrity=\"sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\"></script>
\t\t<script>
\t\t\tvar p = document.getElementById(\"amodal\");
\t\t\tp.onclick = function() { \$(\"#exampleModal\").modal();}; 
\t\t\t\$(document).on('submit', 'form', function (e) { // il est impératif de commencer avec cette méthode qui va empêcher le navigateur d'envoyer le formulaire lui-même
\t\t\te.preventDefault();
\t\t\t\$form = \$(e.target);
\t\t\tdebugger;
\t\t\tvar comment = document.getElementById('comment').value;
\t\t\tvar commentator = document.getElementById('commentator').value;

\t\t\tif (comment == '' || commentator == '') {
\t\t\treturn;
\t\t\t}

\t\t\t\$.ajax({type: \$(this).attr('method'), url: \$(this).attr('action'), data: \$(this).serialize()}).done(function (data) {
\t\t\tif (typeof data.commentator !== 'undefined') { // alert(data.message);
\t\t\tdocument.getElementById('comment').value = '';
\t\t\tdocument.getElementById('commentator').value = '';
\t\t\t\$(\"#exampleModal\").modal();
\t\t\t// document.getElementById(\"myDialog\").showModal();
\t\t\t// alert(data.text);
\t\t\tvar html = \"<div class='card-body'>\" + \"<div class='row'>\" + \"<div class='col-md-2'>\" + \"<img src='https://image.ibb.co/jw55Ex/def_face.jpg' class='img img-rounded img-fluid'/>\" + \"</div>\" + \"<div class='col-md-10'>\" + \"<p class='mb-0'>\" + \"<a href='https://maniruzzaman-akash.blogspot.com/p/contact.html'>\" + \"<strong>\" + data.commentator + \"</strong>\" + \"</a>\" + \"<span class='float-right mr-3 mt-1'>\" + \"<span class='number'>\" + data.likes + \" </span>\" + \"<a href=''>\" + \"<i class='icon-thumbs-o-up'></i>\" + \"</a>\" + \"</span>\" + \"<span class='float-right ml-3 mr-3 mt-1'>\" + \"<span class='number'>\" + data.dislikes + \" </span>\" + \"<i class='icon-thumbs-o-down'></i>\" + \"</span>\" + \"</p>\" + \"<p class='text-secondary mb-2'>\" + \"<span class='seen'>\" + \"<small>à l'instant</small>\" + \"</span>\" + \"</p>\" + \"<p class='mb-0'>\" + data.text + \"</p>\";
\t\t\t\$(\"#cards\").prepend(html);
\t\t\t}
\t\t\t}).fail(function (jqXHR, textStatus, errorThrown) {
\t\t\tif (typeof jqXHR.responseJSON !== 'undefined') {
\t\t\tif (jqXHR.responseJSON.hasOwnProperty('form')) { // \$('#form_body').html(jqXHR.responseJSON.form);
\t\t\t}
\t\t\t// \$('.form_error').html(jqXHR.responseJSON.message);

\t\t\t} else {
\t\t\talert(errorThrown);
\t\t\t}

\t\t\t});

\t\t\t});
</script>{% endblock %}
", "profile/index.html.twig", "/Users/marouane/lebonartisan/templates/profile/index.html.twig");
    }
}
