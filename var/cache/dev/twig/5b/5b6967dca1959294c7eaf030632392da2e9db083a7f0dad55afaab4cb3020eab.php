<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* view_artisans/old */
class __TwigTemplate_b9755744c2bbb8771e89a07a0a32ef7866a736d76a446d8492ac2fa97da22869 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'pageheader' => [$this, 'block_pageheader'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "view_artisans/old"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "view_artisans/old"));

        $this->parent = $this->loadTemplate("base.html.twig", "view_artisans/old", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Nos Artisans
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_pageheader($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageheader"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageheader"));

        // line 7
        echo "    <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/adult-artisan-equipment-634511.jpg"), "html", null, true);
        echo "');\" data-stellar-background-ratio=\"0.7\">
        <div class=\"container\">
            <h1 class=\"mt-5\">Nos Artisans</h1>
            ";
        // line 11
        echo "        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 16
        echo "
    <section class=\"ftco-section services-section bg-primary d-flex justify-content-center py-0\">
        <div class=\"tab-content p-3 w-75\" id=\"v-pills-tabContent\">
            <div class=\"tab-pane fade show active\" id=\"v-pills-1\" role=\"tabpanel\" aria-labelledby=\"v-pills-nextgen-tab\">
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("App\\Controller\\ViewArtisansController:searchArtisansAction"));
        // line 22
        echo "
            </div>
        </div>
    </section>

    <section class=\"ftco-section ftco-candidates ftco-candidates-2 bg-light\">
        <div class=\"row row-no-gutters\">
            <div class=\"container\">
            <div class=\"col-lg-4\">
            </div>
            <div class=\"col-md-12 col-lg-8\">
            </div>
                <div class=\"row d-flex justify-content-center\">
                    ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 35, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 36
            echo "                        <div class=\"w-75\">
                            <div class=\"team d-md-flex mt-3 mb-0 ml-2\">
                                <div class=\"img mr-3 mt-2 ml-2\" style=\"background-image: url(";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/person_1.jpg"), "html", null, true);
            echo ");\"></div>
                                <div class=\"text pl-md-4 mr-3\">
                                    <span class=\"icon-my_location\"></span>
                                    <span>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "city", [], "any", false, false, false, 41), "label", [], "any", false, false, false, 41), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "quartier", [], "any", false, false, false, 41), "html", null, true);
            echo "</span>
                                    <h2>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstname", [], "any", false, false, false, 42), "html", null, true);
            echo "
                                        ";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "lastname", [], "any", false, false, false, 43), "html", null, true);
            echo "</h2>
                                    <span class=\"position\">";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "category", [], "any", false, false, false, 44), "label", [], "any", false, false, false, 44), "html", null, true);
            echo "</span>
                                    <p>";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "description", [], "any", false, false, false, 45), "html", null, true);
            echo "</p>
                                    <span class=\"seen\">Membre depuis
                                        ";
            // line 47
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "createdOn", [], "any", false, false, false, 47), "d/m/Y"), "html", null, true);
            echo "</span>
                                    <p class=\"d-flex justify-content-end\">
                                        <a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_profile", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 49)]), "html", null, true);
            echo "\" class=\"btn btn-primary py-1 w-25\">Consulter le profil</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                </div>
                <div class=\"row mt-5\">
                    <div class=\"col text-center\">
                        <div class=\"block-27\">
                            <ul>
                                <li>
                                    <a href=\"#\">&lt;</a>
                                </li>
                                <li class=\"active\">
                                    <span>1</span>
                                </li>
                                <li>
                                    <a href=\"#\">2</a>
                                </li>
                                <li>
                                    <a href=\"#\">3</a>
                                </li>
                                <li>
                                    <a href=\"#\">4</a>
                                </li>
                                <li>
                                    <a href=\"#\">5</a>
                                </li>
                                <li>
                                    <a href=\"#\">&gt;</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "view_artisans/old";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 55,  184 => 49,  179 => 47,  174 => 45,  170 => 44,  166 => 43,  162 => 42,  156 => 41,  150 => 38,  146 => 36,  142 => 35,  127 => 22,  125 => 20,  119 => 16,  109 => 15,  97 => 11,  90 => 7,  80 => 6,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Nos Artisans
{% endblock %}

{% block pageheader %}
    <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('{{asset('images/adult-artisan-equipment-634511.jpg')}}');\" data-stellar-background-ratio=\"0.7\">
        <div class=\"container\">
            <h1 class=\"mt-5\">Nos Artisans</h1>
            {# <p class=\"lead\">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p> #}
        </div>
    </div>
{% endblock %}

{% block body %}

    <section class=\"ftco-section services-section bg-primary d-flex justify-content-center py-0\">
        <div class=\"tab-content p-3 w-75\" id=\"v-pills-tabContent\">
            <div class=\"tab-pane fade show active\" id=\"v-pills-1\" role=\"tabpanel\" aria-labelledby=\"v-pills-nextgen-tab\">
                {{render(controller(
                    'App\\\\Controller\\\\ViewArtisansController:searchArtisansAction'
                ))}}
            </div>
        </div>
    </section>

    <section class=\"ftco-section ftco-candidates ftco-candidates-2 bg-light\">
        <div class=\"row row-no-gutters\">
            <div class=\"container\">
            <div class=\"col-lg-4\">
            </div>
            <div class=\"col-md-12 col-lg-8\">
            </div>
                <div class=\"row d-flex justify-content-center\">
                    {% for user in results %}
                        <div class=\"w-75\">
                            <div class=\"team d-md-flex mt-3 mb-0 ml-2\">
                                <div class=\"img mr-3 mt-2 ml-2\" style=\"background-image: url({{asset('images/person_1.jpg')}});\"></div>
                                <div class=\"text pl-md-4 mr-3\">
                                    <span class=\"icon-my_location\"></span>
                                    <span>{{ user.city.label }}, {{ user.quartier}}</span>
                                    <h2>{{ user.firstname }}
                                        {{ user.lastname }}</h2>
                                    <span class=\"position\">{{ user.category.label}}</span>
                                    <p>{{ user.description }}</p>
                                    <span class=\"seen\">Membre depuis
                                        {{ user.createdOn |date(\"d/m/Y\")}}</span>
                                    <p class=\"d-flex justify-content-end\">
                                        <a href=\"{{path('view_profile',{'id':user.id})}}\" class=\"btn btn-primary py-1 w-25\">Consulter le profil</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
                <div class=\"row mt-5\">
                    <div class=\"col text-center\">
                        <div class=\"block-27\">
                            <ul>
                                <li>
                                    <a href=\"#\">&lt;</a>
                                </li>
                                <li class=\"active\">
                                    <span>1</span>
                                </li>
                                <li>
                                    <a href=\"#\">2</a>
                                </li>
                                <li>
                                    <a href=\"#\">3</a>
                                </li>
                                <li>
                                    <a href=\"#\">4</a>
                                </li>
                                <li>
                                    <a href=\"#\">5</a>
                                </li>
                                <li>
                                    <a href=\"#\">&gt;</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

        {% endblock %}
", "view_artisans/old", "/Users/marouane/lebonartisan/templates/view_artisans/old");
    }
}
