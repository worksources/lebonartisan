<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/artisanRegistration.html.twig */
class __TwigTemplate_89728e6f5df1dee7dfff30315eb8f8f0739568d6d150c51886866557871adc62 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/artisanRegistration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/artisanRegistration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/artisanRegistration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Inscription
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "\t<div class=\"ftco-section bg-light\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5\">
\t\t\t\t\t";
        // line 11
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), 'form_start', ["attr" => ["class" => "p-5 bg-white", "action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_registration")]]);
        echo "
\t\t\t\t\t<h3 class=\"mb-5\">Créer un compte artisan</h3>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "firstName", [], "any", false, false, false, 15), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 20, $this->source); })()), "lastName", [], "any", false, false, false, 20), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">
\t\t\t\t\t\t\t\tSexe
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 28, $this->source); })()), "sexe", [], "any", false, false, false, 28), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), "sexe", [], "any", false, false, false, 29), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">
\t\t\t\t\t\t\t\tEtes-vous une entreprise ?</label>
\t\t\t\t\t\t\t";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 36, $this->source); })()), "verified", [], "any", false, false, false, 36), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 37, $this->source); })()), "verified", [], "any", false, false, false, 37), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 42, $this->source); })()), "photoFile", [], "any", false, false, false, 42), 'row');
        echo "
\t\t\t\t\t\t\t";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), "photoFile", [], "any", false, false, false, 43), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 48, $this->source); })()), "description", [], "any", false, false, false, 48), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 49, $this->source); })()), "description", [], "any", false, false, false, 49), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 54, $this->source); })()), "hourlyCost", [], "any", false, false, false, 54), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 55, $this->source); })()), "hourlyCost", [], "any", false, false, false, 55), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">Heure d'ouverture</label>
\t\t\t\t\t\t\t";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 61, $this->source); })()), "heureDebut", [], "any", false, false, false, 61), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 62, $this->source); })()), "heureDebut", [], "any", false, false, false, 62), 'errors');
        echo "
                            ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 63, $this->source); })()), "minuteDebut", [], "any", false, false, false, 63), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 64
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 64, $this->source); })()), "minuteDebut", [], "any", false, false, false, 64), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">Heure de fermeture</label>
\t\t\t\t\t\t\t";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 70, $this->source); })()), "heureFin", [], "any", false, false, false, 70), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 71, $this->source); })()), "heureFin", [], "any", false, false, false, 71), 'errors');
        echo "
                            ";
        // line 72
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 72, $this->source); })()), "minuteFin", [], "any", false, false, false, 72), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 73, $this->source); })()), "minuteFin", [], "any", false, false, false, 73), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 78, $this->source); })()), "email", [], "any", false, false, false, 78), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 79, $this->source); })()), "email", [], "any", false, false, false, 79), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 84, $this->source); })()), "password", [], "any", false, false, false, 84), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 85, $this->source); })()), "password", [], "any", false, false, false, 85), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 90
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 90, $this->source); })()), "confirmPassword", [], "any", false, false, false, 90), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 91, $this->source); })()), "confirmPassword", [], "any", false, false, false, 91), 'errors');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 96
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 96, $this->source); })()), "phoneNumber", [], "any", false, false, false, 96), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 101, $this->source); })()), "address", [], "any", false, false, false, 101), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 106
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 106, $this->source); })()), "quartier", [], "any", false, false, false, 106), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 111, $this->source); })()), "city", [], "any", false, false, false, 111), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 116
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 116, $this->source); })()), "category", [], "any", false, false, false, 116), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 117
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 117, $this->source); })()), "_token", [], "any", false, false, false, 117), 'row');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"d-flex justify-content-center mb-3\">
\t\t\t\t\t\t";
        // line 122
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 122, $this->source); })()), "submit", [], "any", false, false, false, 122), 'widget');
        echo "
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>

\t\t\t<div class=\"col-lg-4\">
\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t<h3 class=\"h5 text-black mb-3\">Contact Info</h3>
\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">Address</p>
\t\t\t\t\t<p class=\"mb-4\">203 Fake St. Mountain View, San Francisco, California, USA</p>

\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">Phone</p>
\t\t\t\t\t<p class=\"mb-4\">
\t\t\t\t\t\t<a href=\"#\">+1 232 3235 324</a>
\t\t\t\t\t</p>

\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">Email Address</p>
\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t<span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">[email&#160;protected]</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>

\t\t\t\t</div>

\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t</img>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div></div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/artisanRegistration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  305 => 122,  297 => 117,  293 => 116,  285 => 111,  277 => 106,  269 => 101,  261 => 96,  253 => 91,  249 => 90,  241 => 85,  237 => 84,  229 => 79,  225 => 78,  217 => 73,  213 => 72,  209 => 71,  205 => 70,  196 => 64,  192 => 63,  188 => 62,  184 => 61,  175 => 55,  171 => 54,  163 => 49,  159 => 48,  151 => 43,  147 => 42,  139 => 37,  135 => 36,  125 => 29,  121 => 28,  110 => 20,  102 => 15,  95 => 11,  89 => 7,  79 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Inscription
{% endblock %}

{% block body %}
\t<div class=\"ftco-section bg-light\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5\">
\t\t\t\t\t{{ form_start(form, {'attr': {'class': 'p-5 bg-white', 'action':path('security_registration')}},{'multipart': true}) }}
\t\t\t\t\t<h3 class=\"mb-5\">Créer un compte artisan</h3>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.firstName)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.lastName)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">
\t\t\t\t\t\t\t\tSexe
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t{{form_widget(form.sexe)}}
\t\t\t\t\t\t\t{{form_errors(form.sexe) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">
\t\t\t\t\t\t\t\tEtes-vous une entreprise ?</label>
\t\t\t\t\t\t\t{{form_widget(form.verified)}}
\t\t\t\t\t\t\t{{form_errors(form.verified) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_row(form.photoFile)}}
\t\t\t\t\t\t\t{{form_errors(form.photoFile) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.description)}}
\t\t\t\t\t\t\t{{form_errors(form.description) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.hourlyCost)}}
\t\t\t\t\t\t\t{{form_errors(form.hourlyCost) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">Heure d'ouverture</label>
\t\t\t\t\t\t\t{{form_widget(form.heureDebut)}}
\t\t\t\t\t\t\t{{form_errors(form.heureDebut)}}
                            {{form_widget(form.minuteDebut)}}
\t\t\t\t\t\t\t{{form_errors(form.minuteDebut)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<label class=\"mr-3\">Heure de fermeture</label>
\t\t\t\t\t\t\t{{form_widget(form.heureFin)}}
\t\t\t\t\t\t\t{{form_errors(form.heureFin)}}
                            {{form_widget(form.minuteFin)}}
\t\t\t\t\t\t\t{{form_errors(form.minuteFin)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.email)}}
\t\t\t\t\t\t\t{{form_errors(form.email) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.password)}}
\t\t\t\t\t\t\t{{form_errors(form.password) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.confirmPassword)}}
\t\t\t\t\t\t\t{{form_errors(form.confirmPassword) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.phoneNumber)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.address)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.quartier)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.city)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.category)}}
\t\t\t\t\t\t\t{{form_row(form._token) }}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"d-flex justify-content-center mb-3\">
\t\t\t\t\t\t{{form_widget(form.submit)}}
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>

\t\t\t<div class=\"col-lg-4\">
\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t<h3 class=\"h5 text-black mb-3\">Contact Info</h3>
\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">Address</p>
\t\t\t\t\t<p class=\"mb-4\">203 Fake St. Mountain View, San Francisco, California, USA</p>

\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">Phone</p>
\t\t\t\t\t<p class=\"mb-4\">
\t\t\t\t\t\t<a href=\"#\">+1 232 3235 324</a>
\t\t\t\t\t</p>

\t\t\t\t\t<p class=\"mb-0 font-weight-bold\">Email Address</p>
\t\t\t\t\t<p class=\"mb-0\">
\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t<span class=\"__cf_email__\" data-cfemail=\"671e081215020a060e0b2703080a060e094904080a\">[email&#160;protected]</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>

\t\t\t\t</div>

\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t</img>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div></div>{% endblock %}
", "security/artisanRegistration.html.twig", "/Users/marouane/lebonartisan/templates/security/artisanRegistration.html.twig");
    }
}
