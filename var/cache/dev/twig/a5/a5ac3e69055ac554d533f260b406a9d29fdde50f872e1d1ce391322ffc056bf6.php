<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* view_artisans/index.html.twig */
class __TwigTemplate_ffa05ff6c97183070accf36c3de4e3e2d6d359089a39adb31ead8aacec527fdc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'pageheader' => [$this, 'block_pageheader'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "view_artisans/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "view_artisans/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "view_artisans/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Le Bon Artisan
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_pageheader($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageheader"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageheader"));

        // line 7
        echo "    <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/adult-artisan-equipment-634511.jpg"), "html", null, true);
        echo "');\" data-stellar-background-ratio=\"0.7\">
        <div class=\"container\">
            <h1 class=\"mt-5\">Nos Artisans</h1>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    <section class=\"ftco-candidates ftco-candidates-2 bg-light\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 mt-2\">
                    <div
                        class=\"ftco-search mb-3 mt-4 d-flex justify-content-center \">
                        ";
        // line 22
        echo "                        <div class=\"tab-content w-100 mt-1 shadow\">
                            ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["categoryselect"]) || array_key_exists("categoryselect", $context) ? $context["categoryselect"] : (function () { throw new RuntimeError('Variable "categoryselect" does not exist.', 23, $this->source); })()), 'form_start', ["attr" => ["class" => "search-job"]]);
        echo "
                            <div class=\"row block-9 mt-4 d-flex justify-content-center mb-3\">
                                <div class=\"form-group w-75\">
                                    <div class=\"form-field\">
                                        <div class=\"select-wrap\">
                                            <div class=\"icon\">
                                                <span class=\"ion-ios-arrow-down\"></span>
                                            </div>
                                            ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["categoryselect"]) || array_key_exists("categoryselect", $context) ? $context["categoryselect"] : (function () { throw new RuntimeError('Variable "categoryselect" does not exist.', 31, $this->source); })()), "category", [], "any", false, false, false, 31), 'widget');
        echo "
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"row block-9 d-flex justify-content-center mb-3\">
                                <div class=\"form-group w-75\">
                                    <div class=\"form-field\">
                                        <div class=\"select-wrap\">
                                            <div class=\"icon\">
                                                <span class=\"icon-map-marker\"></span>
                                            </div>
                                            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["categoryselect"]) || array_key_exists("categoryselect", $context) ? $context["categoryselect"] : (function () { throw new RuntimeError('Variable "categoryselect" does not exist.', 43, $this->source); })()), "city", [], "any", false, false, false, 43), 'widget');
        echo "
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"row block-9 mb-4 d-flex justify-content-center\">
                                <div class=\"form-group w-75\">
                                    <div class=\"form-field\">
                                        <button type=\"submit\" class=\"form-control btn btn-secondary\">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                            ";
        // line 55
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["categoryselect"]) || array_key_exists("categoryselect", $context) ? $context["categoryselect"] : (function () { throw new RuntimeError('Variable "categoryselect" does not exist.', 55, $this->source); })()), 'form_end');
        echo "
                        </div>
                    </div>

                    <div class=\"p-4 mb-3 rounded\">
                        <a href=\"\">
                        <img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
                        </img>
                        </a>
                    </div>
                </div>

                <div class=\"col-md-12 col-lg-8\">
                    <div class=\"p-5 mb-5 rounded\">
                        <div class=\"row d-flex justify-content-center\">
                            ";
        // line 70
        if (twig_test_empty((isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 70, $this->source); })()))) {
            // line 71
            echo "                                <h2 class=\"text-black\">Aucun résultat à afficher</h2>
                            ";
        } else {
            // line 73
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 73, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 74
                echo "                                    <div class=\"w-100\">
                                        <div class=\"team d-md-flex ml-2 shadow rounded mb-5 mt-1\">
                                        ";
                // line 76
                if (( !(null === twig_get_attribute($this->env, $this->source, $context["user"], "photo", [], "any", false, false, false, 76)) ||  !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["user"], "photo", [], "any", false, false, false, 76)))) {
                    // line 77
                    echo "                                            <div class=\"img mr-3 mt-3 ml-3 mb-3\" style=\"background-image: url(";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/profile_photos/"), "html", null, true);
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "photo", [], "any", false, false, false, 77), "html", null, true);
                    echo ");\"></div>
                                        ";
                } else {
                    // line 79
                    echo "                                            ";
                    if ((twig_get_attribute($this->env, $this->source, $context["user"], "sexe", [], "any", false, false, false, 79) == 0)) {
                        // line 80
                        echo "                                            <div class=\"img mr-3 mt-3 ml-3 mb-3\" style=\"background-image: url(";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/profile_photos/man.png"), "html", null, true);
                        echo ");\"></div>
                                            ";
                    } else {
                        // line 82
                        echo "                                            <div class=\"img mr-3 mt-3 ml-3 mb-3\" style=\"background-image: url(";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/profile_photos/woman.png"), "html", null, true);
                        echo ");\"></div>
                                            ";
                    }
                    // line 84
                    echo "                                        ";
                }
                // line 85
                echo "                                            <div class=\"text pl-md-4 mr-3 mt-3\">
                                                <h2>";
                // line 86
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "firstname", [], "any", false, false, false, 86), "html", null, true);
                echo "
                                                    ";
                // line 87
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "lastname", [], "any", false, false, false, 87), "html", null, true);
                echo "</h2>
                                                <span class=\"icon-my_location\"></span>
                                                <span>";
                // line 89
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "city", [], "any", false, false, false, 89), "label", [], "any", false, false, false, 89), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "quartier", [], "any", false, false, false, 89), "html", null, true);
                echo "</span>
                                                <span class=\"position\">";
                // line 90
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "category", [], "any", false, false, false, 90), "label", [], "any", false, false, false, 90), "html", null, true);
                echo "</span>
                                                <p>
                                                    ";
                // line 92
                echo twig_escape_filter($this->env, (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "description", [], "any", false, false, false, 92)) > 110)) ? ((twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "description", [], "any", false, false, false, 92), 0, 110) . "...")) : (twig_get_attribute($this->env, $this->source, $context["user"], "description", [], "any", false, false, false, 92))), "html", null, true);
                echo "</p>
                                                <span class=\"seen\">Membre depuis
                                                    ";
                // line 94
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "createdOn", [], "any", false, false, false, 94), "d/m/Y"), "html", null, true);
                echo "</span>
                                                <p class=\"d-flex justify-content-end\">
                                                    <a href=\"";
                // line 96
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_profile", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 96)]), "html", null, true);
                echo "\" class=\"btn btn-primary py-1 w-50\">Consulter le profil</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "                                <div class=\"navigation\">
                                    ";
            // line 103
            echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 103, $this->source); })()));
            echo "
                                </div>
                            ";
        }
        // line 106
        echo "                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "view_artisans/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 106,  277 => 103,  274 => 102,  262 => 96,  257 => 94,  252 => 92,  247 => 90,  241 => 89,  236 => 87,  232 => 86,  229 => 85,  226 => 84,  220 => 82,  214 => 80,  211 => 79,  204 => 77,  202 => 76,  198 => 74,  193 => 73,  189 => 71,  187 => 70,  169 => 55,  154 => 43,  139 => 31,  128 => 23,  125 => 22,  117 => 15,  107 => 14,  90 => 7,  80 => 6,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Le Bon Artisan
{% endblock %}

{% block pageheader %}
    <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('{{asset('images/adult-artisan-equipment-634511.jpg')}}');\" data-stellar-background-ratio=\"0.7\">
        <div class=\"container\">
            <h1 class=\"mt-5\">Nos Artisans</h1>
        </div>
    </div>
{% endblock %}

{% block body %}
    <section class=\"ftco-candidates ftco-candidates-2 bg-light\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 mt-2\">
                    <div
                        class=\"ftco-search mb-3 mt-4 d-flex justify-content-center \">
                        {# {{render(controller('App\\\\Controller\\\\ViewArtisansController:searchArtisansPageAction'))}} #}
                        <div class=\"tab-content w-100 mt-1 shadow\">
                            {{ form_start(categoryselect, {'attr': {'class': 'search-job'}}) }}
                            <div class=\"row block-9 mt-4 d-flex justify-content-center mb-3\">
                                <div class=\"form-group w-75\">
                                    <div class=\"form-field\">
                                        <div class=\"select-wrap\">
                                            <div class=\"icon\">
                                                <span class=\"ion-ios-arrow-down\"></span>
                                            </div>
                                            {{ form_widget(categoryselect.category) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"row block-9 d-flex justify-content-center mb-3\">
                                <div class=\"form-group w-75\">
                                    <div class=\"form-field\">
                                        <div class=\"select-wrap\">
                                            <div class=\"icon\">
                                                <span class=\"icon-map-marker\"></span>
                                            </div>
                                            {{ form_widget(categoryselect.city) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"row block-9 mb-4 d-flex justify-content-center\">
                                <div class=\"form-group w-75\">
                                    <div class=\"form-field\">
                                        <button type=\"submit\" class=\"form-control btn btn-secondary\">Rechercher</button>
                                    </div>
                                </div>
                            </div>
                            {{ form_end(categoryselect) }}
                        </div>
                    </div>

                    <div class=\"p-4 mb-3 rounded\">
                        <a href=\"\">
                        <img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
                        </img>
                        </a>
                    </div>
                </div>

                <div class=\"col-md-12 col-lg-8\">
                    <div class=\"p-5 mb-5 rounded\">
                        <div class=\"row d-flex justify-content-center\">
                            {% if results is empty %}
                                <h2 class=\"text-black\">Aucun résultat à afficher</h2>
                            {% else %}
                                {% for user in results %}
                                    <div class=\"w-100\">
                                        <div class=\"team d-md-flex ml-2 shadow rounded mb-5 mt-1\">
                                        {% if user.photo is not null or user.photo is not empty %}
                                            <div class=\"img mr-3 mt-3 ml-3 mb-3\" style=\"background-image: url({{asset('uploads/profile_photos/')}}{{user.photo}});\"></div>
                                        {% else %}
                                            {% if user.sexe == 0 %}
                                            <div class=\"img mr-3 mt-3 ml-3 mb-3\" style=\"background-image: url({{asset('uploads/profile_photos/man.png')}});\"></div>
                                            {% else %}
                                            <div class=\"img mr-3 mt-3 ml-3 mb-3\" style=\"background-image: url({{asset('uploads/profile_photos/woman.png')}});\"></div>
                                            {% endif %}
                                        {% endif %}
                                            <div class=\"text pl-md-4 mr-3 mt-3\">
                                                <h2>{{ user.firstname }}
                                                    {{ user.lastname }}</h2>
                                                <span class=\"icon-my_location\"></span>
                                                <span>{{ user.city.label }}, {{ user.quartier}}</span>
                                                <span class=\"position\">{{ user.category.label}}</span>
                                                <p>
                                                    {{ user.description|length > 110 ? user.description|slice(0, 110) ~ '...' : user.description  }}</p>
                                                <span class=\"seen\">Membre depuis
                                                    {{ user.createdOn |date(\"d/m/Y\")}}</span>
                                                <p class=\"d-flex justify-content-end\">
                                                    <a href=\"{{path('view_profile',{'id':user.id})}}\" class=\"btn btn-primary py-1 w-50\">Consulter le profil</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                {% endfor %}
                                <div class=\"navigation\">
                                    {{ knp_pagination_render(results) }}
                                </div>
                            {% endif %}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
{% endblock %}
", "view_artisans/index.html.twig", "/Users/marouane/lebonartisan/templates/view_artisans/index.html.twig");
    }
}
