<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_fc0c235b57cdee2e901385f9dee64d55cd22eebb933c96288244c9984cc8d1e8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'links' => [$this, 'block_links'],
            'pageheader' => [$this, 'block_pageheader'],
            'body' => [$this, 'block_body'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <title>
            ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "        </title>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

        <link href=\"https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900\" rel=\"stylesheet\">

        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/open-iconic-bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/animate.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/owl.carousel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/owl.theme.default.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/magnific-popup.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/aos.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/ionicons.min.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/bootstrap-datepicker.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/jquery.timepicker.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/flaticon.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/icomoon.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style.css"), "html", null, true);
        echo "\"> ";
        $this->displayBlock('links', $context, $blocks);
        // line 29
        echo "        </head>
        <body>
            <nav class=\"navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake\" id=\"ftco-navbar\">
                <div class=\"container\">
                    <a class=\"navbar-brand\" href=\"/\"><img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo3.png"), "html", null, true);
        echo "\" alt=\"\" class=\"logo\"></img></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#ftco-nav\" aria-controls=\"ftco-nav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"oi oi-menu\"></span>
                        Menu
                    </button>

                    <div class=\"collapse navbar-collapse\" id=\"ftco-nav\">
                        <ul
                            class=\"navbar-nav ml-auto\">
                            ";
        // line 43
        echo "                            ";
        // line 46
        echo "                            <li class=\"nav-item\">
                                <a href=\"";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("maintenance");
        echo "\" class=\"nav-link\">Aide & Conseils</a>
                            </li>
                            <li class=\"nav-item\">
                                <a href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("maintenance");
        echo "\" class=\"nav-link\">Blog</a>
                            </li>
                            <li class=\"nav-item mr-5\">
                                <a href=\"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\" class=\"nav-link\">Contact</a>
                            </li>
                            ";
        // line 55
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 56
            echo "                                ";
            // line 57
            echo "                                <li class=\"dropdown nav-item mr-3\">
                                    <a class=\"nav-link dropdown-toggle\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        Bonjour
                                        ";
            // line 60
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60), "firstname", [], "any", false, false, false, 60), "html", null, true);
            echo "
                                    </a>
                                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                                        <a href=\"";
            // line 63
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("posts");
            echo "\" class=\"dropdown-item\">
                                            <i class=\"icon-briefcase mr-2\"></i>Annonces à proximité</a>
                                        <a href=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_profile", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 65, $this->source); })()), "user", [], "any", false, false, false, 65), "id", [], "any", false, false, false, 65)]), "html", null, true);
            echo "\" class=\"dropdown-item\">
                                            <i class=\"icon-user mr-2\"></i>Voir votre profil</a>
                                        <a href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_profile", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 67, $this->source); })()), "user", [], "any", false, false, false, 67), "id", [], "any", false, false, false, 67)]), "html", null, true);
            echo "\" class=\"dropdown-item\">
                                            <i class=\"icon-cog mr-2\"></i>Préférences et confidentialité</a>
                                        <a href=\"";
            // line 69
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\" class=\"dropdown-item\">
                                            <i class=\"icon-sign-out mr-2\"></i>Déconnexion</a>
                                    </div>
                                </li>
                            ";
        } else {
            // line 74
            echo "                                <li class=\"nav-item cta mr-3\">
                                    <a href=\"";
            // line 75
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("artisan_auth");
            echo "\" class=\"nav-link\">Espace Artisan</a>
                                </li>
                                <li class=\"nav-item cta cta-colored\">
                                    <a href=\"";
            // line 78
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_registration");
            echo "\" class=\"nav-link\">S'inscrire</a>
                                </li>
                            ";
        }
        // line 81
        echo "                        </ul>
                    </div>
                </div>
            </nav>
            <!-- END nav -->

            ";
        // line 87
        $this->displayBlock('pageheader', $context, $blocks);
        // line 88
        echo "
            ";
        // line 89
        $this->displayBlock('body', $context, $blocks);
        // line 90
        echo "
            <section class=\"ftco-section-parallax\">
                <div class=\"parallax-img d-flex align-items-center\">
                    <div class=\"container\">
                        <div class=\"row d-flex justify-content-center\">
                            <div class=\"col-md-7 text-center heading-section heading-section-white ftco-animate\">
                                <h2>Subcribe to our Newsletter</h2>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
                                <div class=\"row d-flex justify-content-center mt-4 mb-4\">
                                    <div class=\"col-md-12\">
                                        <form action=\"#\" class=\"subscribe-form\">
                                            <div class=\"form-group d-flex\">
                                                <input type=\"text\" class=\"form-control\" placeholder=\"Enter email address\">
                                                <input type=\"submit\" value=\"Subscribe\" class=\"submit px-3\">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <footer class=\"ftco-footer ftco-bg-dark ftco-section\">
                <div class=\"container\">
                    <div class=\"row mb-5\">
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4\">
                                <h2 class=\"ftco-heading-2\">About</h2>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <ul class=\"ftco-footer-social list-unstyled float-md-left float-lft mt-3\">
                                    <li class=\"ftco-animate\">
                                        <a href=\"#\">
                                            <span class=\"icon-twitter\"></span>
                                        </a>
                                    </li>
                                    <li class=\"ftco-animate\">
                                        <a href=\"#\">
                                            <span class=\"icon-facebook\"></span>
                                        </a>
                                    </li>
                                    <li class=\"ftco-animate\">
                                        <a href=\"#\">
                                            <span class=\"icon-instagram\"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4\">
                                <h2 class=\"ftco-heading-2\">Employers</h2>
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">How it works</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Register</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Post a Job</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Advance Skill Search</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Recruiting Service</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Blog</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Faq</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4 ml-md-4\">
                                <h2 class=\"ftco-heading-2\">Workers</h2>
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">How it works</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Register</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Post Your Skills</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Job Search</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Emploer Search</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4\">
                                <h2 class=\"ftco-heading-2\">Have a Questions?</h2>
                                <div class=\"block-23 mb-3\">
                                    <ul>
                                        <li>
                                            <span class=\"icon icon-map-marker\"></span>
                                            <span class=\"text\">203 Fake St. Mountain View, San Francisco, California, USA</span>
                                        </li>
                                        <li>
                                            <a href=\"#\">
                                                <span class=\"icon icon-phone\"></span>
                                                <span class=\"text\">+2 392 3929 210</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"#\">
                                                <span class=\"icon icon-envelope\"></span>
                                                <span class=\"text\">info@yourdomain.com</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12 text-center\">

                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script>
                                All rights reserved | This template is made with
                                <i class=\"icon-heart text-danger\" aria-hidden=\"true\"></i>
                                by
                                <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </footer>


            <!-- loader -->
            <div id=\"ftco-loader\" class=\"show fullscreen\">
                <svg class=\"circular\" width=\"48px\" height=\"48px\"><circle class=\"path-bg\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke=\"#eeeeee\"/><circle class=\"path\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke-miterlimit=\"10\" stroke=\"#F96D00\"/></svg>
            </div>

            ";
        // line 241
        $this->displayBlock('javascript', $context, $blocks);
        // line 242
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery-migrate-3.0.1.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 244
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/popper.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.easing.1.3.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.waypoints.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.stellar.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.magnific-popup.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/aos.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 252
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.animateNumber.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 253
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/scrollax.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false\"></script>
            <script src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/google-map.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>

        </body>
    </html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 28
    public function block_links($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "links"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "links"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 87
    public function block_pageheader($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageheader"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "pageheader"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 89
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 241
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  521 => 241,  503 => 89,  485 => 87,  467 => 28,  449 => 5,  434 => 256,  430 => 255,  425 => 253,  421 => 252,  417 => 251,  413 => 250,  409 => 249,  405 => 248,  401 => 247,  397 => 246,  393 => 245,  389 => 244,  385 => 243,  380 => 242,  378 => 241,  225 => 90,  223 => 89,  220 => 88,  218 => 87,  210 => 81,  204 => 78,  198 => 75,  195 => 74,  187 => 69,  182 => 67,  177 => 65,  172 => 63,  166 => 60,  161 => 57,  159 => 56,  157 => 55,  152 => 53,  146 => 50,  140 => 47,  137 => 46,  135 => 43,  123 => 33,  117 => 29,  113 => 28,  109 => 27,  105 => 26,  100 => 24,  96 => 23,  91 => 21,  86 => 19,  81 => 17,  77 => 16,  73 => 15,  68 => 13,  64 => 12,  56 => 6,  54 => 5,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <title>
            {% block title %}{% endblock %}
        </title>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

        <link href=\"https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900\" rel=\"stylesheet\">

        <link rel=\"stylesheet\" href=\"{{asset('css/open-iconic-bootstrap.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/animate.css')}}\">

        <link rel=\"stylesheet\" href=\"{{asset('css/owl.carousel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/owl.theme.default.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/magnific-popup.css')}}\">

        <link rel=\"stylesheet\" href=\"{{asset('css/aos.css')}}\">

        <link rel=\"stylesheet\" href=\"{{asset('css/ionicons.min.css')}}\">

        <link rel=\"stylesheet\" href=\"{{asset('css/bootstrap-datepicker.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/jquery.timepicker.css')}}\">

        <link rel=\"stylesheet\" href=\"{{asset('css/flaticon.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/icomoon.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/style.css')}}\"> {% block links %}{% endblock %}
        </head>
        <body>
            <nav class=\"navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake\" id=\"ftco-navbar\">
                <div class=\"container\">
                    <a class=\"navbar-brand\" href=\"/\"><img src=\"{{asset('images/logo3.png')}}\" alt=\"\" class=\"logo\"></img></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#ftco-nav\" aria-controls=\"ftco-nav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"oi oi-menu\"></span>
                        Menu
                    </button>

                    <div class=\"collapse navbar-collapse\" id=\"ftco-nav\">
                        <ul
                            class=\"navbar-nav ml-auto\">
                            {# <li class=\"nav-item active\"><a href=\"index.html\" class=\"nav-link\">Home</a></li> #}
                            {# <li class=\"nav-item\">
                                <a href=\"{{path('view_artisans')}}\" class=\"nav-link active\">Artisans</a>
                            </li> #}
                            <li class=\"nav-item\">
                                <a href=\"{{ path('maintenance') }}\" class=\"nav-link\">Aide & Conseils</a>
                            </li>
                            <li class=\"nav-item\">
                                <a href=\"{{ path('maintenance') }}\" class=\"nav-link\">Blog</a>
                            </li>
                            <li class=\"nav-item mr-5\">
                                <a href=\"{{path('contact_us')}}\" class=\"nav-link\">Contact</a>
                            </li>
                            {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                                {# <li class=\"nav-item cta mr-3\"><a href=\"{{path('artisan_auth')}}\" class=\"nav-link\">Espace Artisan</a></li> #}
                                <li class=\"dropdown nav-item mr-3\">
                                    <a class=\"nav-link dropdown-toggle\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        Bonjour
                                        {{ app.user.firstname }}
                                    </a>
                                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                                        <a href=\"{{ path('posts') }}\" class=\"dropdown-item\">
                                            <i class=\"icon-briefcase mr-2\"></i>Annonces à proximité</a>
                                        <a href=\"{{path('view_profile',{'id':app.user.id})}}\" class=\"dropdown-item\">
                                            <i class=\"icon-user mr-2\"></i>Voir votre profil</a>
                                        <a href=\"{{path('view_profile',{'id':app.user.id})}}\" class=\"dropdown-item\">
                                            <i class=\"icon-cog mr-2\"></i>Préférences et confidentialité</a>
                                        <a href=\"{{ path('logout') }}\" class=\"dropdown-item\">
                                            <i class=\"icon-sign-out mr-2\"></i>Déconnexion</a>
                                    </div>
                                </li>
                            {% else %}
                                <li class=\"nav-item cta mr-3\">
                                    <a href=\"{{path('artisan_auth')}}\" class=\"nav-link\">Espace Artisan</a>
                                </li>
                                <li class=\"nav-item cta cta-colored\">
                                    <a href=\"{{path('security_registration')}}\" class=\"nav-link\">S'inscrire</a>
                                </li>
                            {% endif %}
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- END nav -->

            {% block pageheader %}{% endblock %}

            {% block body %}{% endblock %}

            <section class=\"ftco-section-parallax\">
                <div class=\"parallax-img d-flex align-items-center\">
                    <div class=\"container\">
                        <div class=\"row d-flex justify-content-center\">
                            <div class=\"col-md-7 text-center heading-section heading-section-white ftco-animate\">
                                <h2>Subcribe to our Newsletter</h2>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
                                <div class=\"row d-flex justify-content-center mt-4 mb-4\">
                                    <div class=\"col-md-12\">
                                        <form action=\"#\" class=\"subscribe-form\">
                                            <div class=\"form-group d-flex\">
                                                <input type=\"text\" class=\"form-control\" placeholder=\"Enter email address\">
                                                <input type=\"submit\" value=\"Subscribe\" class=\"submit px-3\">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <footer class=\"ftco-footer ftco-bg-dark ftco-section\">
                <div class=\"container\">
                    <div class=\"row mb-5\">
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4\">
                                <h2 class=\"ftco-heading-2\">About</h2>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <ul class=\"ftco-footer-social list-unstyled float-md-left float-lft mt-3\">
                                    <li class=\"ftco-animate\">
                                        <a href=\"#\">
                                            <span class=\"icon-twitter\"></span>
                                        </a>
                                    </li>
                                    <li class=\"ftco-animate\">
                                        <a href=\"#\">
                                            <span class=\"icon-facebook\"></span>
                                        </a>
                                    </li>
                                    <li class=\"ftco-animate\">
                                        <a href=\"#\">
                                            <span class=\"icon-instagram\"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4\">
                                <h2 class=\"ftco-heading-2\">Employers</h2>
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">How it works</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Register</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Post a Job</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Advance Skill Search</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Recruiting Service</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Blog</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Faq</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4 ml-md-4\">
                                <h2 class=\"ftco-heading-2\">Workers</h2>
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">How it works</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Register</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Post Your Skills</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Job Search</a>
                                    </li>
                                    <li>
                                        <a href=\"#\" class=\"py-2 d-block\">Emploer Search</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"col-md\">
                            <div class=\"ftco-footer-widget mb-4\">
                                <h2 class=\"ftco-heading-2\">Have a Questions?</h2>
                                <div class=\"block-23 mb-3\">
                                    <ul>
                                        <li>
                                            <span class=\"icon icon-map-marker\"></span>
                                            <span class=\"text\">203 Fake St. Mountain View, San Francisco, California, USA</span>
                                        </li>
                                        <li>
                                            <a href=\"#\">
                                                <span class=\"icon icon-phone\"></span>
                                                <span class=\"text\">+2 392 3929 210</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"#\">
                                                <span class=\"icon icon-envelope\"></span>
                                                <span class=\"text\">info@yourdomain.com</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12 text-center\">

                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script>
                                All rights reserved | This template is made with
                                <i class=\"icon-heart text-danger\" aria-hidden=\"true\"></i>
                                by
                                <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </footer>


            <!-- loader -->
            <div id=\"ftco-loader\" class=\"show fullscreen\">
                <svg class=\"circular\" width=\"48px\" height=\"48px\"><circle class=\"path-bg\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke=\"#eeeeee\"/><circle class=\"path\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke-miterlimit=\"10\" stroke=\"#F96D00\"/></svg>
            </div>

            {% block javascript %}{% endblock %}
            <script src=\"{{asset('js/jquery.min.js')}}\"></script>
            <script src=\"{{asset('js/jquery-migrate-3.0.1.min.js')}}\"></script>
            <script src=\"{{asset('js/popper.min.js')}}\"></script>
            <script src=\"{{asset('js/bootstrap.min.js')}}\"></script>
            <script src=\"{{asset('js/jquery.easing.1.3.js')}}\"></script>
            <script src=\"{{asset('js/jquery.waypoints.min.js')}}\"></script>
            <script src=\"{{asset('js/jquery.stellar.min.js')}}\"></script>
            <script src=\"{{asset('js/owl.carousel.min.js')}}\"></script>
            <script src=\"{{asset('js/jquery.magnific-popup.min.js')}}\"></script>
            <script src=\"{{asset('js/aos.js')}}\"></script>
            <script src=\"{{asset('js/jquery.animateNumber.min.js')}}\"></script>
            <script src=\"{{asset('js/scrollax.min.js')}}\"></script>
            <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false\"></script>
            <script src=\"{{asset('js/google-map.js')}}\"></script>
            <script src=\"{{asset('js/main.js')}}\"></script>

        </body>
    </html>
", "base.html.twig", "/Users/marouane/lebonartisan/templates/base.html.twig");
    }
}
