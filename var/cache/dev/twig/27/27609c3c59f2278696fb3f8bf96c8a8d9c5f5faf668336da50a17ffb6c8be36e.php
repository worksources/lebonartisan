<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_f66cdcaf8a287a0cc79f969dae69c4565585223c2b2c066779341cc8df5b1b94 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
            <meta charset=\"utf-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
            
            <link href=\"https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"css/open-iconic-bootstrap.min.css\">
            <link rel=\"stylesheet\" href=\"css/animate.css\">          
            <link rel=\"stylesheet\" href=\"css/owl.carousel.min.css\">
            <link rel=\"stylesheet\" href=\"css/owl.theme.default.min.css\">
            <link rel=\"stylesheet\" href=\"css/magnific-popup.css\">
            <link rel=\"stylesheet\" href=\"css/aos.css\">
            <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">
            <link rel=\"stylesheet\" href=\"css/bootstrap-datepicker.css\">
            <link rel=\"stylesheet\" href=\"css/jquery.timepicker.css\">
            <link rel=\"stylesheet\" href=\"css/flaticon.css\">
            <link rel=\"stylesheet\" href=\"css/icomoon.css\">
            <link rel=\"stylesheet\" href=\"css/style.css\">

    </head>
    <body>
        <nav class=\"navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake\" id=\"ftco-navbar\">
          <div class=\"container\">
            <a class=\"navbar-brand\" href=\"/\"><img src=\"images/logo3.png\" alt=\"\" class=\"logo\"></img></a>
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#ftco-nav\" aria-controls=\"ftco-nav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
              <span class=\"oi oi-menu\"></span> Menu
            </button>

            <div class=\"collapse navbar-collapse\" id=\"ftco-nav\">
              <ul class=\"navbar-nav ml-auto\">
                ";
        // line 35
        echo "                ";
        // line 36
        echo "                <li class=\"nav-item\"><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("maintenance");
        echo "\" class=\"nav-link\">Aide & Conseils</a></li>
                <li class=\"nav-item\"><a href=\"";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("maintenance");
        echo "\" class=\"nav-link\">Blog</a></li>
                <li class=\"nav-item mr-5\"><a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\" class=\"nav-link\">Contact</a></li>             
                ";
        // line 39
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 40
            echo "                    ";
            // line 41
            echo "                    <li class=\"dropdown nav-item mr-3\">
                    <a class=\"nav-link dropdown-toggle\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Bonjour ";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "user", [], "any", false, false, false, 43), "firstname", [], "any", false, false, false, 43), "html", null, true);
            echo "
                    </a>
                                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                                    <a href=\"";
            // line 46
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("posts");
            echo "\" class=\"dropdown-item\"><i class=\"icon-briefcase mr-2\"></i>Annonces des clients</a>
                                    <a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_profile", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 47, $this->source); })()), "user", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47)]), "html", null, true);
            echo "\" class=\"dropdown-item\"><i class=\"icon-user mr-2\"></i>Voir votre profil</a>
                                    <a href=\"";
            // line 48
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("artisan_auth");
            echo "\" class=\"dropdown-item\"><i class=\"icon-cog mr-2\"></i>Préférences et confidentialité</a>
                                    <a href=\"";
            // line 49
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\" class=\"dropdown-item\"><i class=\"icon-sign-out mr-2\"></i>Déconnexion</a>
                                </div>
                    </li>
                ";
        } else {
            // line 53
            echo "                    <li class=\"nav-item cta mr-3\"><a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("artisan_auth");
            echo "\" class=\"nav-link\">Espace Artisan</a></li>
                    <li class=\"nav-item cta cta-colored\">
                        <a href=\"";
            // line 55
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_registration");
            echo "\" class=\"nav-link\">S'inscrire</a>
                    </li>
                ";
        }
        // line 58
        echo "              </ul>
            </div>
          </div>
        </nav>
        
        <div class=\"hero-wrap js-fullheight\"style=\"background-image: url('images/build-builder-builders-80921.jpg');\" data-stellar-background-ratio=\"0.5\">
        <div class=\"overlay\"></div>
        <div class=\"container-fluid px-0\">
            <div class=\"row d-md-flex no-gutters slider-text align-items-end js-fullheight justify-content-end\">
                <div class=\"one-forth d-flex align-items-center ftco-animate js-fullheight\">
                    <div class=\"notice p-3 md-5 mt-5 w-100 shadow-lg\" style=\"background-color: rgba(255, 255, 255, 0.7);\">

                        <h2 class=\"mb-5\">Besoin d'un artisan recommandé ?</h2>
                        <p>Grâce à l'intermédiaire vous n'avez plus à vous soucier de trouver une entreprise ou un artisan local.</p>
                        <p> Faites confiance à une communauté de <span class=\"number\" data-number=\"";
        // line 72
        echo twig_escape_filter($this->env, (isset($context["userCount"]) || array_key_exists("userCount", $context) ? $context["userCount"] : (function () { throw new RuntimeError('Variable "userCount" does not exist.', 72, $this->source); })()), "html", null, true);
        echo "\">0</span> professionnels.</p>
                        
                        ";
        // line 79
        echo "
                            <div class=\"ftco-search\">
                            <div class=\"row\">
                            <div class=\"col-md-12 nav-link-wrap mt-2\">
                                <div class=\"nav nav-pills text-center\" id=\"v-pills-tab\" role=\"tablist\" aria-orientation=\"vertical\">
                                ";
        // line 85
        echo "                                ";
        // line 86
        echo "                                </div>
                            </div>
                            <div class=\"col-md-12 tab-wrap mt-2\">
                                
                                <div class=\"tab-content p-3\" id=\"v-pills-tabContent\">
                                <div class=\"tab-pane fade show active\" id=\"v-pills-1\" role=\"tabpanel\" aria-labelledby=\"v-pills-nextgen-tab\">
                                    ";
        // line 92
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("App\\Controller\\ViewArtisansController:searchArtisansAction"));
        // line 94
        echo "
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                </div>
                </div>
                </div>
        </div>
        </div>

        <section class=\"ftco-section services-section bg-primary\">
        <div class=\"container\">
            <div class=\"row d-flex\">
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-promotions\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">DES AVIS 100% FIABLES</h3>
                    <p>Nous avons plusieurs d'entreprises et d'artisans à travers le Maroc qui ont été évalués par leurs clients.</p>
                </div>
                </div>      
            </div>
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-employee\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">UNE ÉQUIPE IMPLIQUÉE</h3>
                    <p>Soyez serein, notre équipe est à votre écoute pour vous conseiller et vous accompagner !</p>
                </div>
                </div>    
            </div>
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-collaboration\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">SIMPLICITÉ</h3>
                    <p>Mise en relation simple rapide et sécurisée</p>
                </div>
                </div>      
            </div>
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-resume\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">DE VRAIS PROS</h3>
                    <p>Toutes les informations sont vérifiées par l'équipe : Siret, téléphone, mail, qualification</p>
                </div>
                </div>      
            </div>
            </div>
        </div>
        </section>

        ";
        // line 193
        echo "
        <section class=\"ftco-section img\" style=\"background-image: url(images/artisan-business-carpenter-carpentry-313776.jpg); background-position: top center;\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-6\">
                    </div>
                    <div class=\"col-md-6\">
                        <div class=\"browse-job p-5\">
                            <h2><span class=\"icon-quote-left\"></span> L'idée</h2>
                            <p>Constatant que la recherche d'un bon artisan pour faire des travaux est anxiogène pour les particuliers, nous proposons la possibilité de commander en ligne son artisan après avoir comparé les prix et les avis des consommateurs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

            ";
        // line 493
        echo "    
        ";
        // line 548
        echo "

        ";
        // line 636
        echo "
        ";
        // line 697
        echo "
        <section class=\"ftco-section bg-light\">
        <div class=\"container\">
            <div class=\"row justify-content-center mb-5 pb-3\">
            <div class=\"col-md-7 heading-section text-center ftco-animate\">
                <span class=\"subheading\">Blog</span>
                <h2><span>Articles</span> récents</h2>
            </div>
            </div>
            <div class=\"row d-flex\">
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_1.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_2.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_3.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_4.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>


        <footer class=\"ftco-footer ftco-bg-dark ftco-section\">
        <div class=\"container\">
            <div class=\"row mb-5\">
                <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4\">
                <h2 class=\"ftco-heading-2\">À propos</h2>
                <p>Constatant que la recherche d'un bon artisan pour faire des travaux est anxiogène pour les particuliers, nous proposons la possibilité de commander en ligne son artisan après avoir comparé les prix et les avis des consommateurs.</p>
                <ul class=\"ftco-footer-social list-unstyled float-md-left float-lft mt-3\">
                    <li class=\"ftco-animate\"><a href=\"#\"><span class=\"icon-twitter\"></span></a></li>
                    <li class=\"ftco-animate\"><a href=\"#\"><span class=\"icon-facebook\"></span></a></li>
                    <li class=\"ftco-animate\"><a href=\"#\"><span class=\"icon-instagram\"></span></a></li>
                </ul>
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4\">
                <h2 class=\"ftco-heading-2\">Clients</h2>
                <ul class=\"list-unstyled\">
                    <li><a href=\"";
        // line 786
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("maintenance");
        echo "\" class=\"py-2 d-block\">Blog</a></li>
                    <li><a href=\"";
        // line 787
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("maintenance");
        echo "\" class=\"py-2 d-block\">Aides & Conseils</a></li>
                    <li><a href=\"";
        // line 788
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("post");
        echo "\" class=\"py-2 d-block\">Déposer une annonce</a></li>
                    <li><a href=\"";
        // line 789
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\" class=\"py-2 d-block\">Contactez-nous</a></li>
                    <li><a href=\"#\" class=\"py-2 d-block\">Faq</a></li>
                </ul>
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4 ml-md-4\">
                <h2 class=\"ftco-heading-2\">Artisans</h2>
                <ul class=\"list-unstyled\">
                    <li><a href=\"";
        // line 798
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_registration");
        echo "\" class=\"py-2 d-block\">S'inscrire</a></li>
                    <li><a href=\"";
        // line 799
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("artisan_auth");
        echo "\" class=\"py-2 d-block\">Espace Artisans</a></li>
                    <li><a href=\"";
        // line 800
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\" class=\"py-2 d-block\">Contactez-nous</a></li>
                </ul>
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4\">
                    <h2 class=\"ftco-heading-2\">Des Questions?</h2>
                    <div class=\"block-23 mb-3\">
                    <ul>
                        <li><span class=\"icon icon-map-marker\"></span><span class=\"text\">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                        <li><a href=\"#\"><span class=\"icon icon-phone\"></span><span class=\"text\">+2 392 3929 210</span></a></li>
                        <li><a href=\"#\"><span class=\"icon icon-envelope\"></span><span class=\"text\">lebonartisan.co@gmail.com</span></a></li>
                    </ul>
                    </div>
                </div>
            </div>
            </div>
            <div class=\"row\">
            <div class=\"col-md-12 text-center\">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --><small>
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small></p>
            </div>
            </div>
        </div>
        </footer>

        <!-- loader -->
        <div id=\"ftco-loader\" class=\"show fullscreen\"><svg class=\"circular\" width=\"48px\" height=\"48px\"><circle class=\"path-bg\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke=\"#eeeeee\"/><circle class=\"path\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke-miterlimit=\"10\" stroke=\"#F96D00\"/></svg></div>
        
        <script src=\"js/jquery.min.js\"></script>
        <script src=\"js/jquery-migrate-3.0.1.min.js\"></script>
        <script src=\"js/popper.min.js\"></script>
        <script src=\"js/bootstrap.min.js\"></script>
        <script src=\"js/jquery.easing.1.3.js\"></script>
        <script src=\"js/jquery.waypoints.min.js\"></script>
        <script src=\"js/jquery.stellar.min.js\"></script>
        <script src=\"js/owl.carousel.min.js\"></script>
        <script src=\"js/jquery.magnific-popup.min.js\"></script>
        <script src=\"js/aos.js\"></script>
        <script src=\"js/jquery.animateNumber.min.js\"></script>
        <script src=\"js/scrollax.min.js\"></script>
        <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false\"></script>
        <script src=\"js/google-map.js\"></script>
        <script src=\"js/main.js\"></script>
    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Le Bon Artisan";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  449 => 5,  390 => 800,  386 => 799,  382 => 798,  370 => 789,  366 => 788,  362 => 787,  358 => 786,  267 => 697,  264 => 636,  260 => 548,  257 => 493,  239 => 193,  182 => 94,  180 => 92,  172 => 86,  170 => 85,  163 => 79,  158 => 72,  142 => 58,  136 => 55,  130 => 53,  123 => 49,  119 => 48,  115 => 47,  111 => 46,  105 => 43,  101 => 41,  99 => 40,  97 => 39,  93 => 38,  89 => 37,  84 => 36,  82 => 35,  50 => 5,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}Le Bon Artisan{% endblock %}</title>
            <meta charset=\"utf-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
            
            <link href=\"https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"css/open-iconic-bootstrap.min.css\">
            <link rel=\"stylesheet\" href=\"css/animate.css\">          
            <link rel=\"stylesheet\" href=\"css/owl.carousel.min.css\">
            <link rel=\"stylesheet\" href=\"css/owl.theme.default.min.css\">
            <link rel=\"stylesheet\" href=\"css/magnific-popup.css\">
            <link rel=\"stylesheet\" href=\"css/aos.css\">
            <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">
            <link rel=\"stylesheet\" href=\"css/bootstrap-datepicker.css\">
            <link rel=\"stylesheet\" href=\"css/jquery.timepicker.css\">
            <link rel=\"stylesheet\" href=\"css/flaticon.css\">
            <link rel=\"stylesheet\" href=\"css/icomoon.css\">
            <link rel=\"stylesheet\" href=\"css/style.css\">

    </head>
    <body>
        <nav class=\"navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake\" id=\"ftco-navbar\">
          <div class=\"container\">
            <a class=\"navbar-brand\" href=\"/\"><img src=\"images/logo3.png\" alt=\"\" class=\"logo\"></img></a>
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#ftco-nav\" aria-controls=\"ftco-nav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
              <span class=\"oi oi-menu\"></span> Menu
            </button>

            <div class=\"collapse navbar-collapse\" id=\"ftco-nav\">
              <ul class=\"navbar-nav ml-auto\">
                {# <li class=\"nav-item active\"><a href=\"index.html\" class=\"nav-link\">Home</a></li> #}
                {# <li class=\"nav-item\"><a href=\"{{path('search_artisans')}}\" class=\"nav-link\">Artisans</a></li> #}
                <li class=\"nav-item\"><a href=\"{{ path('maintenance') }}\" class=\"nav-link\">Aide & Conseils</a></li>
                <li class=\"nav-item\"><a href=\"{{ path('maintenance') }}\" class=\"nav-link\">Blog</a></li>
                <li class=\"nav-item mr-5\"><a href=\"{{path('contact_us')}}\" class=\"nav-link\">Contact</a></li>             
                {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                    {# <li class=\"nav-item cta mr-3\"><a href=\"{{path('artisan_auth')}}\" class=\"nav-link\">Espace Artisan</a></li> #}
                    <li class=\"dropdown nav-item mr-3\">
                    <a class=\"nav-link dropdown-toggle\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Bonjour {{ app.user.firstname }}
                    </a>
                                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                                    <a href=\"{{ path('posts') }}\" class=\"dropdown-item\"><i class=\"icon-briefcase mr-2\"></i>Annonces des clients</a>
                                    <a href=\"{{path('view_profile',{'id':app.user.id})}}\" class=\"dropdown-item\"><i class=\"icon-user mr-2\"></i>Voir votre profil</a>
                                    <a href=\"{{path('artisan_auth')}}\" class=\"dropdown-item\"><i class=\"icon-cog mr-2\"></i>Préférences et confidentialité</a>
                                    <a href=\"{{ path('logout') }}\" class=\"dropdown-item\"><i class=\"icon-sign-out mr-2\"></i>Déconnexion</a>
                                </div>
                    </li>
                {% else %}
                    <li class=\"nav-item cta mr-3\"><a href=\"{{path('artisan_auth')}}\" class=\"nav-link\">Espace Artisan</a></li>
                    <li class=\"nav-item cta cta-colored\">
                        <a href=\"{{path('security_registration')}}\" class=\"nav-link\">S'inscrire</a>
                    </li>
                {% endif %}
              </ul>
            </div>
          </div>
        </nav>
        
        <div class=\"hero-wrap js-fullheight\"style=\"background-image: url('images/build-builder-builders-80921.jpg');\" data-stellar-background-ratio=\"0.5\">
        <div class=\"overlay\"></div>
        <div class=\"container-fluid px-0\">
            <div class=\"row d-md-flex no-gutters slider-text align-items-end js-fullheight justify-content-end\">
                <div class=\"one-forth d-flex align-items-center ftco-animate js-fullheight\">
                    <div class=\"notice p-3 md-5 mt-5 w-100 shadow-lg\" style=\"background-color: rgba(255, 255, 255, 0.7);\">

                        <h2 class=\"mb-5\">Besoin d'un artisan recommandé ?</h2>
                        <p>Grâce à l'intermédiaire vous n'avez plus à vous soucier de trouver une entreprise ou un artisan local.</p>
                        <p> Faites confiance à une communauté de <span class=\"number\" data-number=\"{{userCount}}\">0</span> professionnels.</p>
                        
                        {# <div class=\"d-flex flex-row-reverse mb-3\">
                        <a href=\"new-post.html\" class=\"btn btn-primary py-2 ml-3\">Poster votre annonce</a>
                        <a href=\"new-post.html\" class=\"btn btn-secondary py-2 ml-3\">En savoir plus</a>
                        <p><br></p>
                        </div> #}

                            <div class=\"ftco-search\">
                            <div class=\"row\">
                            <div class=\"col-md-12 nav-link-wrap mt-2\">
                                <div class=\"nav nav-pills text-center\" id=\"v-pills-tab\" role=\"tablist\" aria-orientation=\"vertical\">
                                {# <a class=\"nav-link active mr-md-1\" id=\"v-pills-1-tab\" data-toggle=\"pill\" href=\"#v-pills-1\" role=\"tab\" aria-controls=\"v-pills-1\" aria-selected=\"true\">Trouver un Artisan</a> #}
                                {# <a class=\"nav-link\" id=\"v-pills-2-tab\" data-toggle=\"pill\" href=\"#v-pills-2\" role=\"tab\" aria-controls=\"v-pills-2\" aria-selected=\"false\">Find a Candidate</a> #}
                                </div>
                            </div>
                            <div class=\"col-md-12 tab-wrap mt-2\">
                                
                                <div class=\"tab-content p-3\" id=\"v-pills-tabContent\">
                                <div class=\"tab-pane fade show active\" id=\"v-pills-1\" role=\"tabpanel\" aria-labelledby=\"v-pills-nextgen-tab\">
                                    {{render(controller(
                                        'App\\\\Controller\\\\ViewArtisansController:searchArtisansAction'
                                    ))}}
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                </div>
                </div>
                </div>
        </div>
        </div>

        <section class=\"ftco-section services-section bg-primary\">
        <div class=\"container\">
            <div class=\"row d-flex\">
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-promotions\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">DES AVIS 100% FIABLES</h3>
                    <p>Nous avons plusieurs d'entreprises et d'artisans à travers le Maroc qui ont été évalués par leurs clients.</p>
                </div>
                </div>      
            </div>
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-employee\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">UNE ÉQUIPE IMPLIQUÉE</h3>
                    <p>Soyez serein, notre équipe est à votre écoute pour vous conseiller et vous accompagner !</p>
                </div>
                </div>    
            </div>
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-collaboration\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">SIMPLICITÉ</h3>
                    <p>Mise en relation simple rapide et sécurisée</p>
                </div>
                </div>      
            </div>
            <div class=\"col-md-3 d-flex align-self-stretch ftco-animate\">
                <div class=\"media block-6 services d-block\">
                <div class=\"icon\"><span class=\"flaticon-resume\"></span></div>
                <div class=\"media-body\">
                    <h3 class=\"heading mb-3\">DE VRAIS PROS</h3>
                    <p>Toutes les informations sont vérifiées par l'équipe : Siret, téléphone, mail, qualification</p>
                </div>
                </div>      
            </div>
            </div>
        </div>
        </section>

        {# <section class=\"ftco-section\">
            <div class=\"container\">
                <div class=\"row justify-content-center mb-5 pb-3\">
            <div class=\"col-md-7 heading-section text-center ftco-animate\">
                <span class=\"subheading\">Job Categories</span>
                <h2 class=\"mb-4\">Top Categories</h2>
            </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-3 ftco-animate\">
                    <ul class=\"category\">
                        <li><a href=\"#\">Web Development <br><span class=\"number\">354</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Graphic Designer <br><span class=\"number\">143</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Multimedia <br><span class=\"number\">100</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Advertising <br><span class=\"number\">90</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                    </ul>
                </div>
                <div class=\"col-md-3 ftco-animate\">
                    <ul class=\"category\">
                        <li><a href=\"#\">Education &amp; Training <br><span class=\"number\">100</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">English <br><span class=\"number\">200</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Social Media <br><span class=\"number\">300</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Writing <br><span class=\"number\">150</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                    </ul>
                </div>
                <div class=\"col-md-3 ftco-animate\">
                    <ul class=\"category\">
                        <li><a href=\"#\">PHP Programming <br><span class=\"number\">400</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Project Management <br><span class=\"number\">100</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Finance Management <br><span class=\"number\">222</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Office &amp; Admin <br><span class=\"number\">123</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                    </ul>
                </div>
                <div class=\"col-md-3 ftco-animate\">
                    <ul class=\"category\">
                        <li><a href=\"#\">Web Designer <br><span class=\"number\">324</span> <span>Open position</span></span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Customer Service <br><span class=\"number\">564</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Marketing &amp; Sales <br><span class=\"number\">234</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                        <li><a href=\"#\">Software Development <br><span class=\"number\">425</span> <span>Open position</span><i class=\"ion-ios-arrow-forward\"></i></a></li>
                    </ul>
                </div>
            </div>
            </div>
        </section> #}

        <section class=\"ftco-section img\" style=\"background-image: url(images/artisan-business-carpenter-carpentry-313776.jpg); background-position: top center;\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-6\">
                    </div>
                    <div class=\"col-md-6\">
                        <div class=\"browse-job p-5\">
                            <h2><span class=\"icon-quote-left\"></span> L'idée</h2>
                            <p>Constatant que la recherche d'un bon artisan pour faire des travaux est anxiogène pour les particuliers, nous proposons la possibilité de commander en ligne son artisan après avoir comparé les prix et les avis des consommateurs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

            {# <section class=\"ftco-section bg-light\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-lg-9 pr-lg-5\">
                            <div class=\"row justify-content-center pb-3\">
                    <div class=\"col-md-12 heading-section ftco-animate\">
                        <span class=\"subheading\">Recently Added Jobs</span>
                        <h2 class=\"mb-4\">Hot Jobs</h2>
                    </div>
                    </div>
                            <div class=\"row\">
                                <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Frontend Development</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-primary text-white badge py-2 px-3\">Partime</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">Facebook, Inc.</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                                <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Full Stack Developer</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-warning text-white badge py-2 px-3\">Fulltime</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">Google, Inc.</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                    <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Open Source Interactive Developer</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-info text-white badge py-2 px-3\">Freelance</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">New York Times</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                    <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Frontend Development</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-secondary text-white badge py-2 px-3\">Internship</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">Facebook, Inc.</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                    <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Open Source Interactive Developer</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-danger text-white badge py-2 px-3\">Temporary</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">New York Times</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                        <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Full Stack Developer</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-warning text-white badge py-2 px-3\">Fulltime</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">Google, Inc.</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                    <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Open Source Interactive Developer</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-info text-white badge py-2 px-3\">Freelance</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">New York Times</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                    <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Frontend Development</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-secondary text-white badge py-2 px-3\">Internship</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">Facebook, Inc.</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->

                    <div class=\"col-md-12 ftco-animate\">
                        <div class=\"job-post-item py-4 d-block d-lg-flex align-items-center\">
                        <div class=\"one-third mb-4 mb-md-0\">
                            <div class=\"job-post-item-header d-flex align-items-center\">
                            <h2 class=\"mr-3 text-black\"><a href=\"#\">Open Source Interactive Developer</a></h2>
                            <div class=\"badge-wrap\">
                            <span class=\"bg-danger text-white badge py-2 px-3\">Temporary</span>
                            </div>
                            </div>
                            <div class=\"job-post-item-body d-block d-md-flex\">
                            <div class=\"mr-3\"><span class=\"icon-layers\"></span> <a href=\"#\">New York Times</a></div>
                            <div><span class=\"icon-my_location\"></span> <span>Western City, UK</span></div>
                            </div>
                        </div>

                        <div class=\"one-forth ml-auto d-flex align-items-center mt-4 md-md-0\">
                            <div>
                                <a href=\"#\" class=\"icon text-center d-flex justify-content-center align-items-center icon mr-2\">
                                    <span class=\"icon-heart\"></span>
                                </a>
                            </div>
                            <a href=\"job-single.html\" class=\"btn btn-primary py-2\">Apply Job</a>
                        </div>
                        </div>
                    </div><!-- end -->
                    </div>
                </div>
                <div class=\"col-lg-3 sidebar\">
                    <div class=\"row justify-content-center pb-3\">
                    <div class=\"col-md-12 heading-section ftco-animate\">
                        <span class=\"subheading\">Recruitment agencies</span>
                        <h2 class=\"mb-4\">Top Recruitments</h2>
                    </div>
                    </div>
                    <div class=\"sidebar-box ftco-animate\">
                        <div class=\"border\">
                            <a href=\"#\" class=\"company-wrap\"><img src=\"images/company-1.jpg\" class=\"img-fluid\" alt=\"Colorlib Free Template\"></a>
                            <div class=\"text p-3\">
                                <h3><a href=\"#\">Google Company</a></h3>
                                <p><span class=\"number\">500</span> <span>Open position</span></p>
                            </div>
                        </div>
                    </div>
                    <div class=\"sidebar-box ftco-animate\">
                        <div class=\"border\">
                            <a href=\"#\" class=\"company-wrap\"><img src=\"images/company-2.jpg\" class=\"img-fluid\" alt=\"Colorlib Free Template\"></a>
                            <div class=\"text p-3\">
                                <h3><a href=\"#\">Facebook Company</a></h3>
                                <p><span class=\"number\">700</span> <span>Open position</span></p>
                            </div>
                        </div>
                    </div>
                    <div class=\"sidebar-box ftco-animate\">
                        <div class=\"border\">
                            <a href=\"#\" class=\"company-wrap\"><img src=\"images/company-3.jpg\" class=\"img-fluid\" alt=\"Colorlib Free Template\"></a>
                            <div class=\"text p-3\">
                                <h3><a href=\"#\">IT Programming INC</a></h3>
                                <p><span class=\"number\">700</span> <span>Open position</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </section> #}
    
        {# <section class=\"ftco-section ftco-counter img\" id=\"section-counter\" style=\"background-image: url(images/action-artisan-burnt-construction-1145434.jpg);\" data-stellar-background-ratio=\"0.5\">
            <div class=\"container\">
                <div class=\"row justify-content-center\">
                    <div class=\"col-md-12\">
                        <div class=\"row\">
                    <div class=\"col-md-3 d-flex justify-content-center counter-wrap ftco-animate\">
                        <div class=\"block-18 text-center\">
                        <div class=\"text\">
                            <div class=\"icon\">
                                <span class=\"flaticon-employee\"></span>
                            </div>
                            <strong class=\"number\" data-number=\"435000\">0</strong>
                            <span>Jobs</span>
                        </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 d-flex justify-content-center counter-wrap ftco-animate\">
                        <div class=\"block-18 text-center\">
                        <div class=\"text\">
                            <div class=\"icon\">
                                <span class=\"flaticon-collaboration\"></span>
                            </div>
                            <strong class=\"number\" data-number=\"40000\">0</strong>
                            <span>Members</span>
                        </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 d-flex justify-content-center counter-wrap ftco-animate\">
                        <div class=\"block-18 text-center\">
                        <div class=\"text\">
                            <div class=\"icon\">
                                <span class=\"flaticon-resume\"></span>
                            </div>
                            <strong class=\"number\" data-number=\"30000\">0</strong>
                            <span>Resume</span>
                        </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 d-flex justify-content-center counter-wrap ftco-animate\">
                        <div class=\"block-18 text-center\">
                        <div class=\"text\">
                            <div class=\"icon\">
                                <span class=\"flaticon-promotions\"></span>
                            </div>
                            <strong class=\"number\" data-number=\"10500\">0</strong>
                            <span>Company</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </section> #}


        {# <section class=\"ftco-section testimony-section\">
        <div class=\"container\">
            <div class=\"row justify-content-center mb-5 pb-3\">
            <div class=\"col-md-7 text-center heading-section ftco-animate\">
                <span class=\"subheading\">Testimonial</span>
                <h2 class=\"mb-4\">Happy Clients</h2>
            </div>
            </div>
            <div class=\"row ftco-animate\">
            <div class=\"col-md-12\">
                <div class=\"carousel-testimony owl-carousel ftco-owl\">
                <div class=\"item\">
                    <div class=\"testimony-wrap py-4 pb-5\">
                    <div class=\"user-img mb-4\" style=\"background-image: url(images/person_1.jpg)\">
                        <span class=\"quote d-flex align-items-center justify-content-center\">
                        <i class=\"icon-quote-left\"></i>
                        </span>
                    </div>
                    <div class=\"text\">
                        <p class=\"mb-4\">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class=\"name\">Roger Scott</p>
                        <span class=\"position\">Marketing Manager</span>
                    </div>
                    </div>
                </div>
                <div class=\"item\">
                    <div class=\"testimony-wrap py-4 pb-5\">
                    <div class=\"user-img mb-4\" style=\"background-image: url(images/person_2.jpg)\">
                        <span class=\"quote d-flex align-items-center justify-content-center\">
                        <i class=\"icon-quote-left\"></i>
                        </span>
                    </div>
                    <div class=\"text\">
                        <p class=\"mb-4\">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class=\"name\">Roger Scott</p>
                        <span class=\"position\">Interface Designer</span>
                    </div>
                    </div>
                </div>
                <div class=\"item\">
                    <div class=\"testimony-wrap py-4 pb-5\">
                    <div class=\"user-img mb-4\" style=\"background-image: url(images/person_3.jpg)\">
                        <span class=\"quote d-flex align-items-center justify-content-center\">
                        <i class=\"icon-quote-left\"></i>
                        </span>
                    </div>
                    <div class=\"text\">
                        <p class=\"mb-4\">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class=\"name\">Roger Scott</p>
                        <span class=\"position\">UI Designer</span>
                    </div>
                    </div>
                </div>
                <div class=\"item\">
                    <div class=\"testimony-wrap py-4 pb-5\">
                    <div class=\"user-img mb-4\" style=\"background-image: url(images/person_1.jpg)\">
                        <span class=\"quote d-flex align-items-center justify-content-center\">
                        <i class=\"icon-quote-left\"></i>
                        </span>
                    </div>
                    <div class=\"text\">
                        <p class=\"mb-4\">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class=\"name\">Roger Scott</p>
                        <span class=\"position\">Web Developer</span>
                    </div>
                    </div>
                </div>
                <div class=\"item\">
                    <div class=\"testimony-wrap py-4 pb-5\">
                    <div class=\"user-img mb-4\" style=\"background-image: url(images/person_1.jpg)\">
                        <span class=\"quote d-flex align-items-center justify-content-center\">
                        <i class=\"icon-quote-left\"></i>
                        </span>
                    </div>
                    <div class=\"text\">
                        <p class=\"mb-4\">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class=\"name\">Roger Scott</p>
                        <span class=\"position\">System Analyst</span>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section> #}

        {# <section class=\"ftco-section ftco-candidates bg-primary\">
            <div class=\"container\">
                <div class=\"row justify-content-center pb-3\">
            <div class=\"col-md-10 heading-section heading-section-white text-center ftco-animate\">
                <span class=\"subheading\">Candidates</span>
                <h2 class=\"mb-4\">Latest Candidates</h2>
            </div>
            </div>
            </div>
            <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12 ftco-animate\">
                    <div class=\"carousel-candidates owl-carousel\">
                        <div class=\"item\">
                            <a href=\"#\" class=\"team text-center\">
                                <div class=\"img\" style=\"background-image: url(images/person_1.jpg);\"></div>
                                <h2>Danica Lewis</h2>
                                <span class=\"position\">Western City, UK</span>
                            </a>
                        </div>
                        <div class=\"item\">
                            <a href=\"#\" class=\"team text-center\">
                                <div class=\"img\" style=\"background-image: url(images/person_2.jpg);\"></div>
                                <h2>Nicole Simon</h2>
                                <span class=\"position\">Western City, UK</span>
                            </a>
                        </div>
                        <div class=\"item\">
                            <a href=\"#\" class=\"team text-center\">
                                <div class=\"img\" style=\"background-image: url(images/person_3.jpg);\"></div>
                                <h2>Cloe Meyer</h2>
                                <span class=\"position\">Western City, UK</span>
                            </a>
                        </div>
                        <div class=\"item\">
                            <a href=\"#\" class=\"team text-center\">
                                <div class=\"img\" style=\"background-image: url(images/person_4.jpg);\"></div>
                                <h2>Rachel Clinton</h2>
                                <span class=\"position\">Western City, UK</span>
                            </a>
                        </div>
                        <div class=\"item\">
                            <a href=\"#\" class=\"team text-center\">
                                <div class=\"img\" style=\"background-image: url(images/person_5.jpg);\"></div>
                                <h2>Dave Buff</h2>
                                <span class=\"position\">Western City, UK</span>
                            </a>
                        </div>
                        <div class=\"item\">
                            <a href=\"#\" class=\"team text-center\">
                                <div class=\"img\" style=\"background-image: url(images/person_6.jpg);\"></div>
                                <h2>Dave Buff</h2>
                                <span class=\"position\">Western City, UK</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section> #}

        <section class=\"ftco-section bg-light\">
        <div class=\"container\">
            <div class=\"row justify-content-center mb-5 pb-3\">
            <div class=\"col-md-7 heading-section text-center ftco-animate\">
                <span class=\"subheading\">Blog</span>
                <h2><span>Articles</span> récents</h2>
            </div>
            </div>
            <div class=\"row d-flex\">
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_1.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_2.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_3.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            <div class=\"col-md-3 d-flex ftco-animate\">
                <div class=\"blog-entry align-self-stretch\">
                <a href=\"blog-single.html\" class=\"block-20\" style=\"background-image: url('images/image_4.jpg');\">
                </a>
                <div class=\"text mt-3\">
                    <div class=\"meta mb-2\">
                    <div><a href=\"#\">May 3, 2019</a></div>
                    <div><a href=\"#\">Admin</a></div>
                    <div><a href=\"#\" class=\"meta-chat\"><span class=\"icon-chat\"></span> 3</a></div>
                    </div>
                    <h3 class=\"heading\"><a href=\"#\">Disponible prochainement ...</a></h3>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>


        <footer class=\"ftco-footer ftco-bg-dark ftco-section\">
        <div class=\"container\">
            <div class=\"row mb-5\">
                <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4\">
                <h2 class=\"ftco-heading-2\">À propos</h2>
                <p>Constatant que la recherche d'un bon artisan pour faire des travaux est anxiogène pour les particuliers, nous proposons la possibilité de commander en ligne son artisan après avoir comparé les prix et les avis des consommateurs.</p>
                <ul class=\"ftco-footer-social list-unstyled float-md-left float-lft mt-3\">
                    <li class=\"ftco-animate\"><a href=\"#\"><span class=\"icon-twitter\"></span></a></li>
                    <li class=\"ftco-animate\"><a href=\"#\"><span class=\"icon-facebook\"></span></a></li>
                    <li class=\"ftco-animate\"><a href=\"#\"><span class=\"icon-instagram\"></span></a></li>
                </ul>
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4\">
                <h2 class=\"ftco-heading-2\">Clients</h2>
                <ul class=\"list-unstyled\">
                    <li><a href=\"{{ path('maintenance') }}\" class=\"py-2 d-block\">Blog</a></li>
                    <li><a href=\"{{ path('maintenance') }}\" class=\"py-2 d-block\">Aides & Conseils</a></li>
                    <li><a href=\"{{ path('post') }}\" class=\"py-2 d-block\">Déposer une annonce</a></li>
                    <li><a href=\"{{ path('contact_us') }}\" class=\"py-2 d-block\">Contactez-nous</a></li>
                    <li><a href=\"#\" class=\"py-2 d-block\">Faq</a></li>
                </ul>
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4 ml-md-4\">
                <h2 class=\"ftco-heading-2\">Artisans</h2>
                <ul class=\"list-unstyled\">
                    <li><a href=\"{{path('security_registration')}}\" class=\"py-2 d-block\">S'inscrire</a></li>
                    <li><a href=\"{{path('artisan_auth')}}\" class=\"py-2 d-block\">Espace Artisans</a></li>
                    <li><a href=\"{{ path('contact_us') }}\" class=\"py-2 d-block\">Contactez-nous</a></li>
                </ul>
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"ftco-footer-widget mb-4\">
                    <h2 class=\"ftco-heading-2\">Des Questions?</h2>
                    <div class=\"block-23 mb-3\">
                    <ul>
                        <li><span class=\"icon icon-map-marker\"></span><span class=\"text\">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                        <li><a href=\"#\"><span class=\"icon icon-phone\"></span><span class=\"text\">+2 392 3929 210</span></a></li>
                        <li><a href=\"#\"><span class=\"icon icon-envelope\"></span><span class=\"text\">lebonartisan.co@gmail.com</span></a></li>
                    </ul>
                    </div>
                </div>
            </div>
            </div>
            <div class=\"row\">
            <div class=\"col-md-12 text-center\">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --><small>
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small></p>
            </div>
            </div>
        </div>
        </footer>

        <!-- loader -->
        <div id=\"ftco-loader\" class=\"show fullscreen\"><svg class=\"circular\" width=\"48px\" height=\"48px\"><circle class=\"path-bg\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke=\"#eeeeee\"/><circle class=\"path\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke-miterlimit=\"10\" stroke=\"#F96D00\"/></svg></div>
        
        <script src=\"js/jquery.min.js\"></script>
        <script src=\"js/jquery-migrate-3.0.1.min.js\"></script>
        <script src=\"js/popper.min.js\"></script>
        <script src=\"js/bootstrap.min.js\"></script>
        <script src=\"js/jquery.easing.1.3.js\"></script>
        <script src=\"js/jquery.waypoints.min.js\"></script>
        <script src=\"js/jquery.stellar.min.js\"></script>
        <script src=\"js/owl.carousel.min.js\"></script>
        <script src=\"js/jquery.magnific-popup.min.js\"></script>
        <script src=\"js/aos.js\"></script>
        <script src=\"js/jquery.animateNumber.min.js\"></script>
        <script src=\"js/scrollax.min.js\"></script>
        <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false\"></script>
        <script src=\"js/google-map.js\"></script>
        <script src=\"js/main.js\"></script>
    </body>
</html>
", "home/index.html.twig", "/Users/marouane/lebonartisan/templates/home/index.html.twig");
    }
}
