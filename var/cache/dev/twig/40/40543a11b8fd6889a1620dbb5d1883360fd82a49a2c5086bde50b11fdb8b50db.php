<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/posts.html.twig */
class __TwigTemplate_053a7778b40ffb905dc0cc5d595e8d250f559a0ec43452e0fc6406404dcdd2d3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/posts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/posts.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "post/posts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Déposez votre annonce
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "\t<div class=\"ftco-candidates ftco-candidates-2 bg-light mt-5\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5 mt-5\">
\t\t\t\t\t<div class=\"p-5 rounded\">
\t\t\t\t\t\t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t\t\t";
        // line 13
        if (twig_test_empty((isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 13, $this->source); })()))) {
            // line 14
            echo "\t\t\t\t\t\t\t\t<h2 class=\"text-black\">Aucun résultat à afficher</h2>
\t\t\t\t\t\t\t";
        } else {
            // line 16
            echo "\t\t\t\t\t\t\t\t<h2 class=\"text-black mb-4\">Annonces à proximité</h2>
\t\t\t\t\t\t\t\t";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 17, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 18
                echo "\t\t\t\t\t\t\t\t\t<div class=\"w-100\">
\t\t\t\t\t\t\t\t\t\t<div class=\"team d-md-flex ml-2 rounded mb-4 mt-1 shadow-lg\">
\t\t\t\t\t\t\t\t\t\t\t";
                // line 20
                if (( !(null === twig_get_attribute($this->env, $this->source, $context["post"], "photo1", [], "any", false, false, false, 20)) ||  !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["post"], "photo1", [], "any", false, false, false, 20)))) {
                    // line 21
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_post", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 21)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"img rounded float-left mr-2 mt-2 ml-2 mb-2\" style=\"background-image: url(";
                    // line 22
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("uploads/post_photos/"), "html", null, true);
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "photo1", [], "any", false, false, false, 22), "html", null, true);
                    echo ");\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 25
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_post", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 25)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"img rounded float-left mr-2 mt-2 ml-2 mb-2\" style=\"background-image: url(";
                    // line 26
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/gallery.png"), "html", null, true);
                    echo ");\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 29
                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"text pl-md-4 mr-3 w-100 mt-3\">
                                                <a href=\"";
                // line 30
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("view_post", ["id" => twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 30)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"text-black mb-3 mt-2\">";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 31), "html", null, true);
                echo "</h2>
                                                </a>
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"icon-my_location\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t<span>";
                // line 34
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "city", [], "any", false, false, false, 34), "label", [], "any", false, false, false, 34), "html", null, true);
                echo "</span><br>
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"position\">";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "category", [], "any", false, false, false, 35), "label", [], "any", false, false, false, 35), "html", null, true);
                echo "</span><br>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 38
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"seen\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 39
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "createdOn", [], "any", false, false, false, 39), "d/m/Y, H:i"), "html", null, true);
                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "\t\t\t\t\t\t\t";
        }
        // line 45
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-4 mt-5\">
\t\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t</img>
\t\t\t\t\t</a>
\t\t\t\t</div>

\t\t\t\t<div class=\"p-4 mb-3 bg-white d-flex d-flex justify-content-center\">
\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t</img>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div></div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "post/posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 45,  174 => 44,  163 => 39,  160 => 38,  156 => 35,  152 => 34,  146 => 31,  142 => 30,  139 => 29,  133 => 26,  128 => 25,  121 => 22,  116 => 21,  114 => 20,  110 => 18,  106 => 17,  103 => 16,  99 => 14,  97 => 13,  89 => 7,  79 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Déposez votre annonce
{% endblock %}

{% block body %}
\t<div class=\"ftco-candidates ftco-candidates-2 bg-light mt-5\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5 mt-5\">
\t\t\t\t\t<div class=\"p-5 rounded\">
\t\t\t\t\t\t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t\t\t{% if results is empty %}
\t\t\t\t\t\t\t\t<h2 class=\"text-black\">Aucun résultat à afficher</h2>
\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<h2 class=\"text-black mb-4\">Annonces à proximité</h2>
\t\t\t\t\t\t\t\t{% for post in results %}
\t\t\t\t\t\t\t\t\t<div class=\"w-100\">
\t\t\t\t\t\t\t\t\t\t<div class=\"team d-md-flex ml-2 rounded mb-4 mt-1 shadow-lg\">
\t\t\t\t\t\t\t\t\t\t\t{% if post.photo1 is not null or post.photo1 is not empty %}
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{path('view_post',{'id':post.id})}}\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"img rounded float-left mr-2 mt-2 ml-2 mb-2\" style=\"background-image: url({{asset('uploads/post_photos/')}}{{post.photo1}});\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{path('view_post',{'id':post.id})}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"img rounded float-left mr-2 mt-2 ml-2 mb-2\" style=\"background-image: url({{asset('images/gallery.png')}});\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t<div class=\"text pl-md-4 mr-3 w-100 mt-3\">
                                                <a href=\"{{path('view_post',{'id':post.id})}}\">
\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"text-black mb-3 mt-2\">{{ post.title }}</h2>
                                                </a>
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"icon-my_location\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t<span>{{ post.city.label }}</span><br>
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"position\">{{ post.category.label}}</span><br>
\t\t\t\t\t\t\t\t\t\t\t\t{# <p>
\t\t\t\t\t\t\t\t\t\t\t\t\t{{ post.description|length > 110 ? post.description|slice(0, 110) ~ '...' : post.description  }}</p> #}
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"seen\">
\t\t\t\t\t\t\t\t\t\t\t\t\t{{ post.createdOn |date(\"d/m/Y, H:i\")}}</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-4 mt-5\">
\t\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t</img>
\t\t\t\t\t</a>
\t\t\t\t</div>

\t\t\t\t<div class=\"p-4 mb-3 bg-white d-flex d-flex justify-content-center\">
\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t</img>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div></div>{% endblock %}
", "post/posts.html.twig", "/Users/marouane/lebonartisan/templates/post/posts.html.twig");
    }
}
