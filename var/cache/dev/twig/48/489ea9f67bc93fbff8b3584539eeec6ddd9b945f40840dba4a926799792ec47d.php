<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/index.html.twig */
class __TwigTemplate_bfee2585b58b40bd099ace84c08113952b27564e42238a628fcee00eb097b890 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "post/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Déposez votre annonce
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "\t<div class=\"ftco-section bg-light\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5\">
\t\t\t\t\t";
        // line 11
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), 'form_start', ["attr" => ["class" => "p-5 bg-white rounded"]]);
        echo "
\t\t\t\t\t<h3 class=\"mb-1\">Déposez une annonce</h3>
\t\t\t\t\t<hr>
\t\t\t\t\t<h5 class=\"mb-3 mt-5\">
\t\t\t\t\t\t<i class=\"icon-address-card-o mr-3\"></i>Vos coordonnées</h5>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "firstname", [], "any", false, false, false, 18), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "lastname", [], "any", false, false, false, 23), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 28, $this->source); })()), "email", [], "any", false, false, false, 28), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "phone", [], "any", false, false, false, 33), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<h5 class=\"mb-3 mt-5\">
\t\t\t\t\t\t<i class=\"icon-briefcase mr-3\"></i>Informations sur les travaux</h5>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 40, $this->source); })()), "title", [], "any", false, false, false, 40), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 45, $this->source); })()), "city", [], "any", false, false, false, 45), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), "category", [], "any", false, false, false, 50), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 55, $this->source); })()), "description", [], "any", false, false, false, 55), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°1</label>
\t\t\t\t\t\t\t";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 61, $this->source); })()), "photoFile1", [], "any", false, false, false, 61), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°2</label>
\t\t\t\t\t\t\t";
        // line 67
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 67, $this->source); })()), "photoFile2", [], "any", false, false, false, 67), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°3</label>
\t\t\t\t\t\t\t";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 73, $this->source); })()), "photoFile3", [], "any", false, false, false, 73), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°4</label>
\t\t\t\t\t\t\t";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 79, $this->source); })()), "photoFile4", [], "any", false, false, false, 79), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"d-flex justify-content-center mt-4\">
\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary  py-2 px-4\" value=\"Poster\">Déposer votre annonce</button>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 85
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 85, $this->source); })()), 'form_end');
        echo "

\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-4\">
\t\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t</img>
\t\t\t\t\t</a>
\t\t\t\t</div>

\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t</img>
\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "post/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 85,  199 => 79,  190 => 73,  181 => 67,  172 => 61,  163 => 55,  155 => 50,  147 => 45,  139 => 40,  129 => 33,  121 => 28,  113 => 23,  105 => 18,  95 => 11,  89 => 7,  79 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Déposez votre annonce
{% endblock %}

{% block body %}
\t<div class=\"ftco-section bg-light\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 col-lg-8 mb-5\">
\t\t\t\t\t{{form_start(form, {'attr': {'class': 'p-5 bg-white rounded'}}) }}
\t\t\t\t\t<h3 class=\"mb-1\">Déposez une annonce</h3>
\t\t\t\t\t<hr>
\t\t\t\t\t<h5 class=\"mb-3 mt-5\">
\t\t\t\t\t\t<i class=\"icon-address-card-o mr-3\"></i>Vos coordonnées</h5>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.firstname)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.lastname)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.email)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.phone)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<h5 class=\"mb-3 mt-5\">
\t\t\t\t\t\t<i class=\"icon-briefcase mr-3\"></i>Informations sur les travaux</h5>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.title)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.city)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.category)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t{{form_widget(form.description)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°1</label>
\t\t\t\t\t\t\t{{form_widget(form.photoFile1)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°2</label>
\t\t\t\t\t\t\t{{form_widget(form.photoFile2)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°3</label>
\t\t\t\t\t\t\t{{form_widget(form.photoFile3)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t<div class=\"col-md-12 mb-3 mb-md-0\">
\t\t\t\t\t\t\t<lable class=\"mr-4\">Photo N°4</label>
\t\t\t\t\t\t\t{{form_widget(form.photoFile4)}}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"d-flex justify-content-center mt-4\">
\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary  py-2 px-4\" value=\"Poster\">Déposer votre annonce</button>
\t\t\t\t\t</div>
\t\t\t\t\t{{form_end(form)}}

\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-4\">
\t\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t\t</img>
\t\t\t\t\t</a>
\t\t\t\t</div>

\t\t\t\t<div class=\"p-4 mb-3 bg-white\">
\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t<img class=\"w-100 rounded\" src=\"https://lh6.googleusercontent.com/proxy/vEyR_3xzk84rrST3V-4ug6W9hivtKmYj98RzlJsn-bxG4peo7LGRNSU1ZIQnxcp_S5k=s0-d\">
\t\t\t\t\t</img>
\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>{% endblock %}
", "post/index.html.twig", "/Users/marouane/lebonartisan/templates/post/index.html.twig");
    }
}
