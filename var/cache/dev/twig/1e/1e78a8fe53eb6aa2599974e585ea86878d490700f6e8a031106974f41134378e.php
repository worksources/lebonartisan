<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact/index.html.twig */
class __TwigTemplate_bd3835e1b74ac66301a5b99fba30840c915fc79686ac221a643f9642052483b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contact/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Contactez nous
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section class=\"ftco-section contact-section bg-light\">
        <div class=\"container\">
            <div class=\"row d-flex mb-5 contact-info rounded shadow-lg\" style=\"background-image: url('";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/contact.png"), "html", null, true);
        echo "');\">
                <div class=\"col-md-12 mb-4\">
                    <h2 class=\"h3 ml-3 mt-3\">Nos coordonnées</h2>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col-md-3 mb-2 ml-2\">
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <i class=\"icon-instagram\"></i>
                        </div>
                        <div class=\"col-md\">
                            <a href=\"\">
                                @lebonartisan</a>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <i class=\"icon-facebook\"></i>
                        </div>
                        <div class=\"col-md\">
                            <a href=\"\">
                                lebonartisan</a>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <i class=\"icon-envelope\"></i>
                        </div>
                        <div class=\"col-md\">
                            <a href=\"\">
                                lebonartisan@infos.ma</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row block-9\">
                <div class=\"col-md-6 order-md-last d-flex\">
                    <form action=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_us");
        echo "\" class=\"bg-white p-5 contact-form shadow-lg rounded\" method=\"post\">
                        <p>
                            <strong>Contactez-nous</strong>
                        </p>
                        <div class=\"form-group\">
                            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), "fullname", [], "any", false, false, false, 50), 'widget');
        echo "
                        </div>
                        <div class=\"form-group\">
                            ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 53, $this->source); })()), "email", [], "any", false, false, false, 53), 'widget');
        echo "
                        </div>
                        <div class=\"form-group\">
                            ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 56, $this->source); })()), "subject", [], "any", false, false, false, 56), 'widget');
        echo "
                        </div>
                        <div class=\"form-group\">
                            ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 59, $this->source); })()), "message", [], "any", false, false, false, 59), 'widget');
        echo "
                            ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 60, $this->source); })()), "_token", [], "any", false, false, false, 60), 'row');
        echo "
                        </div>
                        <div class=\"form-group d-flex justify-content-center\">
                            <input type=\"submit\" value=\"Envoyer\" class=\"btn btn-primary py-3 px-5\">
                        </div>
                    </form>

                </div>

                <div class=\"col-md-6 d-flex\">
                    <div class=\"bg-white w-100 shadow-lg rounded\">
                        <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5730.344991866796!2d-7.582004544407112!3d33.53734195132566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda632e9741e9c3f%3A0xdb1016cab4c71bfa!2sBoulevard%20d&#39;Amgala%2C%20Casablanca%2C%20Maroc!5e0!3m2!1sfr!2sfr!4v1573169225935!5m2!1sfr!2sfr\" class=\"w-100 h-100\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 60,  159 => 59,  153 => 56,  147 => 53,  141 => 50,  133 => 45,  93 => 8,  89 => 6,  79 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Contactez nous
{% endblock %}
{% block body %}
    <section class=\"ftco-section contact-section bg-light\">
        <div class=\"container\">
            <div class=\"row d-flex mb-5 contact-info rounded shadow-lg\" style=\"background-image: url('{{asset('images/contact.png')}}');\">
                <div class=\"col-md-12 mb-4\">
                    <h2 class=\"h3 ml-3 mt-3\">Nos coordonnées</h2>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col-md-3 mb-2 ml-2\">
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <i class=\"icon-instagram\"></i>
                        </div>
                        <div class=\"col-md\">
                            <a href=\"\">
                                @lebonartisan</a>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <i class=\"icon-facebook\"></i>
                        </div>
                        <div class=\"col-md\">
                            <a href=\"\">
                                lebonartisan</a>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <i class=\"icon-envelope\"></i>
                        </div>
                        <div class=\"col-md\">
                            <a href=\"\">
                                lebonartisan@infos.ma</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row block-9\">
                <div class=\"col-md-6 order-md-last d-flex\">
                    <form action=\"{{path('contact_us')}}\" class=\"bg-white p-5 contact-form shadow-lg rounded\" method=\"post\">
                        <p>
                            <strong>Contactez-nous</strong>
                        </p>
                        <div class=\"form-group\">
                            {{form_widget(form.fullname)}}
                        </div>
                        <div class=\"form-group\">
                            {{form_widget(form.email)}}
                        </div>
                        <div class=\"form-group\">
                            {{form_widget(form.subject)}}
                        </div>
                        <div class=\"form-group\">
                            {{form_widget(form.message)}}
                            {{form_row(form._token) }}
                        </div>
                        <div class=\"form-group d-flex justify-content-center\">
                            <input type=\"submit\" value=\"Envoyer\" class=\"btn btn-primary py-3 px-5\">
                        </div>
                    </form>

                </div>

                <div class=\"col-md-6 d-flex\">
                    <div class=\"bg-white w-100 shadow-lg rounded\">
                        <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5730.344991866796!2d-7.582004544407112!3d33.53734195132566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda632e9741e9c3f%3A0xdb1016cab4c71bfa!2sBoulevard%20d&#39;Amgala%2C%20Casablanca%2C%20Maroc!5e0!3m2!1sfr!2sfr!4v1573169225935!5m2!1sfr!2sfr\" class=\"w-100 h-100\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
", "contact/index.html.twig", "/Users/marouane/lebonartisan/templates/contact/index.html.twig");
    }
}
