<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\ArtisanUser;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ArtisanUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArtisanUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArtisanUser[]    findAll()
 * @method ArtisanUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtisanUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArtisanUser::class);
    }

    // /**
    //  * @return ArtisanUser[] Returns an array of ArtisanUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArtisanUser
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

     /**
     * @return Query
     */
     public function findBySearchQuery(string $city, int $category): Query
     {
         $queryBuilder = $this->createQueryBuilder('p');
        
         $queryBuilder
         ->where('p.city = :searchTerm')
         ->andWhere('p.category ='.$category)
         ->setParameter('searchTerm', $city);

         return $queryBuilder
             ->orderBy('p.createdOn', 'DESC')
             ->getQuery();
     }

    /**
     * @return Query
     */
    public function findByCategoryAndCity($city,$category)
    {

        $query = $this->createQueryBuilder('a')
                      ->select('a')
                      ->leftJoin('a.categories', 'c')
                      ->addSelect('c');
 
        $query = $query->add('where', $query->expr()->in('c', ':c'))
                      ->setParameter('c', $category)
                      ->andWhere('a.city ='.$city)
                      ->orderBy('a.createdOn', 'DESC')
                      ->getQuery();
          
        return $query;
    }

     /**
     * @return ArtisanUser
     */
    public function findByEmail(string $email)
    {
        $queryBuilder = $this->createQueryBuilder('p');
       
        $queryBuilder
        ->Where('p.email = :searchTerm')
        ->setParameter('searchTerm', $email);

        return $queryBuilder
            ->getQuery()
            ->getResult();
    }

     public function getCountUsers(): int
     {
        $queryBuilder = $this->createQueryBuilder('u');
        
        return $queryBuilder
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
     }

    /**
    * @return ArtisanUser[] Returns an array of ArtisanUser objects
    */
    public function findLatestArtisans($nb)
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.createdOn', 'DESC')
            ->setMaxResults($nb)
            ->getQuery()
            ->getResult()
        ;
    }
     

}
