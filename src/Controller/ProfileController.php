<?php

namespace App\Controller;

use DateTime;
use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\ArtisanUserRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index()
    {
        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
        ]);
    }

     /**
     * @Route("/comment/new", name="submit_comment")
     * @Method({"GET", "POST"})
     */
    public function commentNewAction(Request $request, ObjectManager $entityManager,ArtisanUserRepository $repo)
    {
        if ($request->isXMLHttpRequest()) 
        {
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceLimit(2);
            // Add Circular reference handler
            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId();
            });
            $normalizers = array($normalizer);
            $serializer = new Serializer($normalizers, $encoders);
            $content = $request->getContent();
            if (!empty($content)) {
                $params = json_decode($content, true);
                //dump($params);
                $comment = new Comment();
                $comment->setText($request->request->get('comment'));
                $comment->setCreatedOn(new DateTime());
                $comment->setLikes(0);
                $comment->setDislikes(0);
                $comment->setCommentator($request->request->get('name'));
                $id = $request->request->get('id');
                $artisan = $repo->find($id);
                $comment->setAbout($artisan);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($comment);
                $entityManager->flush();
            }
            $jsonContent = $comment->jsonSerialize();
            //dump($jsonContent);
            return new JsonResponse($jsonContent, 200);
        }

        return new JsonResponse('Error!', 400);
    }

     /**
     * @Route("/subcomment/new", name="submit_subcomment")
     * @Method({"GET", "POST"})
     */
    public function subCommentNewAction(Request $request, ObjectManager $entityManager,ArtisanUserRepository $repo,CommentRepository $repocom)
    {
        if ($request->isXMLHttpRequest()) 
        {
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceLimit(2);
            // Add Circular reference handler
            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId();
            });
            $normalizers = array($normalizer);
            $serializer = new Serializer($normalizers, $encoders);
            $content = $request->getContent();
            if (!empty($content)) {
                $params = json_decode($content, true);
                dump($params);
                $comment = new Comment();
                $comment->setText($request->request->get('subcomment'));
                $comment->setCreatedOn(new DateTime());
                $comment->setLikes(0);
                $comment->setDislikes(0);
                $id = $request->request->get('idartisan');
                $artisan = $repo->find($id);
                $comment->setCommentator($artisan->getFirstName());
                $comment->setAbout($artisan);
                $idcomment = $request->request->get('idcomment');
                $replyTo = $repocom->find($idcomment);
                $comment->setReplyTo($replyTo);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($comment);
                $entityManager->flush();
            }
            $jsonContent = $comment->jsonSerialize();
            //dump($jsonContent);
            return new JsonResponse($jsonContent, 200);
        }

        return new JsonResponse('Error!', 400);
    }
}
