<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Category;
use App\Entity\ArtisanUser;
use App\Entity\ArtisanSearch;
use App\Form\ArtisanSearchType;
use Doctrine\ORM\EntityRepository;
use App\Repository\ArtisanUserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ViewArtisansController extends Controller
{
    private $city;
    private $category;

    /**
     * @Route("/artisans", name="view_artisans")
     */
    public function index()
    {
        $results = array();
        
        return $this->render('view_artisans/index.html.twig', [
            'controller_name' => 'ViewArtisansController',
            'results' => $results
        ]);
    }

    public function searchArtisansAction()
    {
        $search = new ArtisanSearch();
        $search_form = $this->createForm(ArtisanSearchType::class,$search);
        
        return $this->render('view_artisans/searchForm.html.twig', [
            'categoryselect' => $search_form->createView()
        ]);
    }

    public function searchArtisansPageAction()
    {
        $search_form = $this->createFormBuilder(null)
                ->add('category', EntityType::class,[
                    // looks for choices from this entity
                    'class' => Category::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.label', 'ASC');
                    },
                    'placeholder' => 'Catégorie',
                    'attr' => ['class' => 'form-control'],
                    'label' => false,
                    'choice_label' => 'label',
                ])
                ->add('city', EntityType::class,[
                    // looks for choices from this entity
                    'class' => City::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.label', 'ASC');
                    },
                    'placeholder' => 'Ville',
                    'attr' => ['class' => 'form-control'],
                    'label' => false,
                    'choice_label' => 'label',
                ])
                ->getForm();
        
        return $this->render('view_artisans/searchFormPage.html.twig', [
            'categoryselect' => $search_form->createView()
        ]);
    }

    /**
     * @Route("/searchartisans", name="search_artisans")
     * @param Request $request
     * @ return Response
     */
    public function searchHandler(Request $request, ArtisanUserRepository $repo, PaginatorInterface $paginator): Response
    {       
        //dump(empty($request->request->get('form')));        
        $search = new ArtisanSearch();
        $search_form = $this->createForm(ArtisanSearchType::class,$search);

        //from my page
        if(empty($request->request->get('form')))
        {
            $search_form->handleRequest($request);
            //dump($search_form['city']->getData()->getId());
            $city = $search_form['city']->getData()->getId();
            $categoryid = $search_form['category']->getData()->getId();
        }
        //from home
        else
        {
            $city = $request->request->get('form')['city'];
            $categoryid = $request->request->get('form')['category'];
            $cityObject = $this->getDoctrine()
                               ->getRepository(City::class)
                               ->find($city);
            $categoryObject = $this->getDoctrine()
                                   ->getRepository(Category::class)
                                   ->find($categoryid);
            $search->setCity($cityObject);
            $search->setCategory($categoryObject);
            //dump($search);
            $search_form = $this->createForm(ArtisanSearchType::class,$search);
            $search_form->handleRequest($request);
        }

        //$query = $repo->findBySearchQuery($city,$categoryid);

        $query = $repo->findByCategoryAndCity($city,$categoryid);

        //dump($query->getResult());

        $paginator  = $this->get('knp_paginator');
        $results = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );

        return $this->render('view_artisans/index.html.twig', [
            'results' => $results,
            'city' => $city,
            'category' => $categoryid,
            'categoryselect' => $search_form->createView()
        ]);

        // dump($results);
        // die();
    }

    public function viewArtisansAction()
    {    
        return $this->render('view_artisans/viewResults.html.twig', [
            'categoryselect' => $this->$results
        ]);
    }

    /**
     * @Route("/view/profile/{id}", name="view_profile")
     */
    public function showProfile($id)
    {
        $repo = $this->getDoctrine()->getRepository(ArtisanUser::class);
        $artisan = $repo->find($id);
        $form = $this->createFormBuilder($artisan)
                ->add('firstName', TextType::class,[              
                    'attr' => [
                        'placeholder' => 'Prénom*',
                        'class' => 'form-control'],
                    'label' => 'Prénom',
                ])
                ->add('lastName', TextType::class,[                
                    'attr' => [
                        'placeholder' => 'Nom*',
                        'class' => 'form-control'],
                    'label' => 'Nom',
                ])
                ->add('photo', FileType::class, [
                    'label' => 'Brochure (PDF file)',

                    // unmapped means that this field is not associated to any entity property
                    'mapped' => false,

                    // make it optional so you don't have to re-upload the PDF file
                    // everytime you edit the Product details
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Photo de profil',
                        'class' => 'form-control-file'],
                    // unmapped fields can't define their validation using annotations
                    // in the associated entity, so you can use the PHP constraint classes
                    'constraints' => [
                        new File([
                            'maxSize' => '1024k',
                            'mimeTypes' => [
                                'application/pdf',
                                'application/x-pdf',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid JPG,PNG file',
                        ])
                    ],
                ])
                ->add('description', TextareaType::class,[
                    'attr' => [
                        'placeholder' => 'Décrivez votre activité',
                        'class' => 'form-control'],
                    'label' => 'Description',
                ])
                ->add('hourlyCost', NumberType::class,[
                    'attr' => [
                        'placeholder' => 'Coût horaire*',
                        'class' => 'form-control tinymce',],
                    'label' => 'Coût horaire',
                ])
                ->add('email', EmailType::class,[
                    'attr' => [
                        'placeholder' => 'Adresse e-mail*',
                        'class' => 'form-control',
                        'disabled' => 'disabled'],
                    'label' => 'E-mail',
                ])
                ->add('password', PasswordType::class,[
                    'attr' => [
                        'placeholder' => 'Mot de passe',
                        'class' => 'form-control'],
                    'label' => true,
                ])
                ->add('confirmPassword', PasswordType::class,[
                    'attr' => [
                        'placeholder' => 'Confirmation du mot de passe',
                        'class' => 'form-control'],
                    'label' => false,
                ])
                ->add('phoneNumber', TelType::class,[
                    'attr' => [
                        'placeholder' => 'Numéro de téléphone*',
                        'class' => 'form-control'],
                    'label' => false,
                ])
                ->add('address', TextareaType::class,[
                    'attr' => [
                        'placeholder' => 'Adresse postale*',
                        'class' => 'form-control'],
                    'label' => false,
                ])
                ->add('city', EntityType::class,[
                    'class' => City::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy(' c.label', 'ASC');
                    },
                    'placeholder' => 'Ville*',
                    'attr' => ['class' => 'form-control'],
                    'label' => false,
                    'choice_label' => 'label',
                ])
                ->add('category', EntityType::class,[
                    // looks for choices from this entity
                    'class' => Category::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.label', 'ASC');
                    },
                    'placeholder' => 'Catégorie*',
                    'attr' => ['class' => 'form-control'],
                    'label' => false,
                    'choice_label' => 'label',
                ]);
        $form = $form->getForm();
        return $this->render('profile/index.html.twig', [
            'form' => $form->createView(),
            'artisan' => $artisan
        ]);
    }
}
