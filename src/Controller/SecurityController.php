<?php

namespace App\Controller;

use DateTime;
use App\Entity\ArtisanUser;
use App\Form\ArtisanUpdateType;
use App\Form\ArtisanRegistrationType;
use App\Repository\ArtisanUserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{

    /**
     * @Route("/register", name="security_registration")
     */
    public function register(\Swift_Mailer $mailer,Request $request, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $entityManager)
    {
        // 1) build the form
        $user = new ArtisanUser();
        $form = $this->createForm(ArtisanRegistrationType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            //dump($form);
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setCreatedOn(new DateTime());
            $user->setLikes(0);
            $user->setDislikes(0);
            $user->setVerified(false);

            if(array_key_exists("isEntreprise",$request->request->get('artisan_registration')))
            {
                dump($request->request->get('artisan_registration'));
                $isEntreprise = $request->request->get('artisan_registration')['isEntreprise'];
                if($isEntreprise == 1)
                {
                    $user->setIsEntreprise(true);
                }
                else
                {
                    $user->setIsEntreprise(false);
                }
            }
            else
            {
                $user->setIsEntreprise(false);
            }
            //$user->setHeureFin($request->request->get('form')['city']);
            //$user->setHeureDebut(false);
            $uploadedFile = $request->files->get('artisan_registration')['photoFile'];
            //var_dump($request->files);die;
            $token = $this->generateUniqueFileName();
            $user->setToken($token);

            if($uploadedFile)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('artisan_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setPhoto($fileName);
            }

            $uploadedFile1 = $request->files->get('artisan_registration')['photoFile1'];
            if($uploadedFile1)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile1->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile1->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto1($fileName);
            }

            $uploadedFile2 = $request->files->get('artisan_registration')['photoFile2'];
            if($uploadedFile2)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile2->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile2->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto2($fileName);
            }

            $uploadedFile3 = $request->files->get('artisan_registration')['photoFile3'];
            if($uploadedFile3)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile3->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile3->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto3($fileName);
            }

            $uploadedFile4 = $request->files->get('artisan_registration')['photoFile4'];
            if($uploadedFile4)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile4->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile4->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto4($fileName);
            }
            
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $activationurl = "https://www.lebonartisan.ma/activation/".$token."/".urlencode($user->getEmail());

            $message = "Bonjour ".$user->getFirstName().",\n\n
            Nous vous confirmons la création de votre compte ".$user->getEmail()."\n
            Pour compléter votre inscription, cliquez sur le lien ci-dessous :\n".$activationurl."
            \nCela nous confirmera que vous êtes bien le propriétaire de cette adresse e-mail.\n
            Si vous n'avez pas demandé à créer de compte, vous pouvez simplement ignorer cet e-mail.";
            
            $message = (new \Swift_Message("Votre compte a été créé avec succès !"))
                ->setFrom("lebonartisan.co@gmail.com")
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'email_templates/activation.html.twig',
                        ['message' => $message,
                        'fullname' => $user->getFirstname()." ".$user->getLastname(),
                        'email' => $user->getEmail(),
                        'subject' => ""]
                    ),'text/html');
        
            $mailer->send($message);
            
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->render(
                'security/confirmation.html.twig'
            );
        }

        return $this->render(
            'security/artisanRegistration.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/update", name="update_profile")
     */
    public function update(\Swift_Mailer $mailer,Request $request, ArtisanUserRepository $repo, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $entityManager)
    {
        // 1) build the form
        $user = $this->getUser();
        $form = $this->createForm(ArtisanUpdateType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user->setCreatedOn(new DateTime());
            $user->setLikes(0);
            $user->setDislikes(0);
            //dump($request);
            if(array_key_exists("isEntreprise",$request->request->get('artisan_update')))
            {
                dump($request->request->get('artisan_update'));
                $isEntreprise = $request->request->get('artisan_update')['isEntreprise'];
                if($isEntreprise == 1)
                {
                    $user->setIsEntreprise(true);
                }
                else
                {
                    $user->setIsEntreprise(false);
                }
            }
            else
            {
                $user->setIsEntreprise(false);
            }

            $uploadedFile1 = $request->files->get('artisan_update')['photoFile1'];
            if($uploadedFile1)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile1->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile1->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto1($fileName);
            }

            $uploadedFile2 = $request->files->get('artisan_update')['photoFile2'];
            if($uploadedFile2)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile2->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile2->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto2($fileName);
            }

            $uploadedFile3 = $request->files->get('artisan_update')['photoFile3'];
            if($uploadedFile3)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile3->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile3->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto3($fileName);
            }

            $uploadedFile4 = $request->files->get('artisan_update')['photoFile4'];
            if($uploadedFile4)
            {
                $fileName = $this->generateUniqueFileName().'.'.$uploadedFile4->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile4->move(
                        $this->getParameter('work_uploads_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $user->setWorkphoto4($fileName);
            }

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('redirect_profil');
        }
        $pathimg1 = $this->getParameter('work_uploads_directory').'/'.$user->getWorkphoto1();
        $img1 = file_get_contents($pathimg1); 
        // Encode the image string data into base64 
        $data1 = base64_encode($img1);

        $pathimg2 = $this->getParameter('work_uploads_directory').'/'.$user->getWorkphoto2();
        $img2 = file_get_contents($pathimg2); 
        // Encode the image string data into base64 
        $data2 = base64_encode($img2);

        $pathimg3 = $this->getParameter('work_uploads_directory').'/'.$user->getWorkphoto3();
        $img3 = file_get_contents($pathimg3); 
        // Encode the image string data into base64 
        $data3 = base64_encode($img3);

        $pathimg4 = $this->getParameter('work_uploads_directory').'/'.$user->getWorkphoto4();
        $img4 = file_get_contents($pathimg4); 
        // Encode the image string data into base64 
        $data4 = base64_encode($img4);
        
        return $this->render(
            'security/artisanUpdate.html.twig',
            array('form' => $form->createView(),
            'img1' => $data1,
            'img2' => $data2,
            'img3' => $data3,
            'img4' => $data4)
        );
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * @Route("/login", name="artisan_auth")
     */
    public function login(AuthenticationUtils $authUtils)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)
            return $this->redirectToRoute('home');
        }
        else
        {
            return $this->render('artisan_auth/index.html.twig',
            array('error' => $error));
        }
    }

    /**
     * @Route("/redirect/profil", name="redirect_profil")
     */
    public function redirectProfil()
    {
        $user = $this->getUser();

        if($user->getVerified() == 0)
        {
            return $this->redirectToRoute('logout');
        }
        else
        {
            return $this->redirectToRoute('view_profile' , 
            ['id' => $user-> getId()]);
        }
        
    }

    /**
     * @Route("/forgotpassword", name="forgot_password")
     */
    public function forgetPassword()
    {
        
        return $this->render('security/forgotpassword.html.twig');
    }

    /**
     * @Route("/requestnewpassword", name="new_password"))
     * @param Request $request
     */
    public function sendNewPassword(\Swift_Mailer $mailer, Request $request, ArtisanUserRepository $repo)
    {
        //dump($request->query->get('email'));
        
        $email = $request->query->get('email');
        $email = urldecode($email);
        $artisan = $repo->findByEmail($email);
        
        if(count($artisan) != 0)
        {
            dump($artisan);
            $activationurl = "https://www.lebonartisan.ma/resetpassword/".$artisan[0]->getToken()."/".urlencode($artisan[0]->getEmail());
            $message = "Bonjour ".$artisan[0]->getFirstName().",\n\n
            Pour compléter votre demande de modification du mot de passe, cliquez sur le lien ci-dessous :\n\n".$activationurl."
            \nCela nous confirmera que vous êtes bien à l'origine de cette demande.\n
            Si vous n'avez pas fait cette demande, vous pouvez simplement ignorer cet e-mail.";
            
            $message = (new \Swift_Message("Votre nouveau mot de passe !"))
                ->setFrom("lebonartisan.co@gmail.com")
                ->setTo($artisan[0]->getEmail())
                ->setBody(
                    $this->renderView(
                        'email_templates/activation.html.twig',
                        ['message' => $message,
                        'fullname' => $artisan[0]->getFirstname()." ".$artisan[0]->getLastname(),
                        'email' => $artisan[0]->getEmail(),
                        'subject' => ""]
                    ),'text/html');
        
            $mailer->send($message);

            return $this->render(
                'security/confirmation.html.twig'
            );
        }

        return $this->render(
            'security/confirmation.html.twig'
        );
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        return $this->render('artisan_auth/index.html.twig');
    }

    /**
     * @Route("/activation/{token}/{email}", methods={"GET","HEAD"})
     */
    public function findByEmail($token,$email)
    {
        //dump($token);
        $repo = $this->getDoctrine()->getRepository(ArtisanUser::class);
        $email = urldecode($email);
        //dump($email);
        $artisan = $repo->findByEmail($email);
        

        if(count($artisan) != 0)
        {
            //dump($artisan[0]);
            if($artisan[0]->getToken() == $token)
            {
                $artisan[0]->setVerified(1);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($artisan[0]);
                $entityManager->flush();
                return $this->render('profile/confirmation.html.twig');
            }
        }

        return $this->render('maintenance/index.html.twig');
    }

    /**
     * @Route("/resetpassword/{token}/{email}", methods={"GET","HEAD"})
     */
    public function resetPassword($token,$email)
    {
        //dump($token);
        $repo = $this->getDoctrine()->getRepository(ArtisanUser::class);
        $email = urldecode($email);
        dump($email);
        $artisan = $repo->findByEmail($email);
        
        if(count($artisan) != 0)
        {
            //dump($artisan[0]);
            if($artisan[0]->getToken() == $token)
            {
                $form = $this->createFormBuilder($artisan)
                    ->add('password', PasswordType::class,[
                        'attr' => [
                            'placeholder' => 'Mot de passe',
                            'class' => 'form-control'],
                        'label' => true,
                    ])
                    ->add('confirmPassword', PasswordType::class,[
                        'attr' => [
                            'placeholder' => 'Confirmation du mot de passe',
                            'class' => 'form-control'],
                        'label' => false,
                    ])
                    ->add('submit', SubmitType::class,[
                        'attr' => ['class' => 'btn btn-primary  py-2 px-4 w-50 mt-3'],
                        'label' => 'Mettre à jour'
                    ]);

                $form = $form->getForm();

                return $this->render('security/resetpassword.html.twig', [
                    'form' => $form->createView(),
                    'artisan' => $artisan[0]
                ]);
            }
        }
        
        return $this->render('maintenance/index.html.twig');
    }

    /**
     * @Route("/resetpasswordaction", name="reset_pswd")
     */
    public function resetPasswordAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $entityManager)
    {
        $id = $request->request->get('id');
        $password = $request->request->get('form')['password'];

        $repo = $this->getDoctrine()->getRepository(ArtisanUser::class);
        $artisan = $repo->find($id);
        //dump($id);
        $password = $passwordEncoder->encodePassword($artisan, $password);
        $artisan->setPassword($password);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($artisan);
        $entityManager->flush();

        return $this->render(
            'security/confirmation.html.twig'
        );
    }
}
