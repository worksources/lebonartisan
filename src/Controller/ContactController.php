<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contactus", name="contact_us")
     */
    public function contactUs(\Swift_Mailer $mailer, Request $request)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //dump($form);
            $fullname = $form['fullname']->getData();
            $subject = $form['subject']->getData();
            $email = $form['email']->getData();
            $message = $form['message']->getData();
            $message = (new \Swift_Message("Nouveau message de ".$fullname))
                ->setFrom($email)
                ->setTo('maous.marouane@gmail.com')
                ->setBody(
                    $this->renderView(
                        'email_templates/new_message.html.twig',
                        ['message' => $message,
                        'fullname' => $fullname,
                        'email' => $email,
                        'subject' => $subject]
                    ),'text/html');
            
            $mailer->send($message);
            return $this->redirectToRoute('home');
        }

        return $this->render(
            'contact/index.html.twig',
            array('form' => $form->createView())
        );
    }

}
