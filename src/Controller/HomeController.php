<?php

namespace App\Controller;

use App\Entity\ArtisanUser;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

class HomeController extends AbstractController
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    /**
     * @Route("/", name="home")
     */
    public function index(ObjectManager $entityManager, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userCount = $entityManager->getRepository(ArtisanUser::class)->getCountUsers();
        $latestArtisans = $entityManager->getRepository(ArtisanUser::class)->findLatestArtisans(5);

        
        
        //dump($request->getLocale());

        $response = new Response(
            'Content',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
        
        $response->prepare($request);
        $cookies = $request->cookies;
        

        if (!$cookies->has('_locale'))
        {
            $localeCookie = false;
            $language = $this->session->get('_locale', 'fr_FR');
        }
        else
        {
            //$language = $this->session->get('_locale', 'fr_FR');
            $language = $cookies->get('_locale');
            $this->session->set('_locale', $language);
            $localeCookie = true;
        }

        dump($language);
        $this->session->save();

        //dump($this->session);
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'userCount' => $userCount,
            'latest' => $latestArtisans,
            'language' => $language,
            'localeCookie' => $localeCookie
        ]);
    }

    /**
     * @Route("/404", name="maintenance")
     */
    public function maintenance()
    {
        return $this->render('maintenance/index.html.twig', [
            'controller_name' => 'HomeController'
        ]);
    }

    /**
     * @Route("/ar/", name="arab")
     */
    public function setArab(Request $request)
    {
        $response = new Response(
            'Content',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );

        $response->prepare($request);
        $cookies = $request->cookies;

        //dump($this->session);
        //$this->session->set('_locale', 'ar_AR');
        $response->headers->setCookie(new Cookie('_locale', 'ar_AR'));
        $this->session->set('_locale', 'ar_AR');
        $response->send();
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/fr/", name="french")
     */
    public function setFrench(Request $request)
    {
        $response = new Response(
            'Content',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );

        $response->prepare($request);
        $cookies = $request->cookies;

        //dump($this->session);
        //$this->session->set('_locale', 'ar_AR');
        $response->headers->setCookie(new Cookie('_locale', 'fr_FR'));
        $this->session->set('_locale', 'fr_FR');
        $response->send();
        return $this->redirectToRoute('home');
    }
}
