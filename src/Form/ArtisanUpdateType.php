<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Category;
use App\Entity\ArtisanUser;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArtisanUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Prénom*',
                    'class' => 'form-control'],
                'label' => 'Prénom',
            ])
            ->add('lastName', TextType::class,[                
                'attr' => [
                    'placeholder' => 'Nom*',
                    'class' => 'form-control'],
                'label' => 'Nom',
            ])
            ->add('sexe', ChoiceType::class, [
                'choices'  => [
                    'Homme' => 0,
                    'Femme' => 1,
                ],
            ])
            ->add('isEntreprise', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Etes vous une entreprise ?',
                'label' => true,
                'required' => false,
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'placeholder' => 'Décrivez votre activité',
                    'class' => 'form-control'],
                'label' => 'Description',
                'label' => true,
            ])
            ->add('hourlyCost', NumberType::class,[
                'attr' => [
                    'placeholder' => 'Coût horaire*',
                    'class' => 'form-control tinymce',],
                'label' => 'Coût horaire',
            ])
            ->add('password', PasswordType::class,[
                'attr' => [
                    'placeholder' => 'Mot de passe',
                    'class' => 'form-control'],
                'mapped' => false,
            ])
            ->add('email', EmailType::class,[
                'attr' => [
                    'placeholder' => 'Adresse e-mail*',
                    'class' => 'form-control'],
                'label' => 'E-mail',
                'disabled' => true,
                'required' => true,
            ])
            ->add('phoneNumber', TelType::class,[
                'attr' => [
                    'placeholder' => 'Numéro de téléphone*',
                    'class' => 'form-control'],
                'label' => false,
            ])
            ->add('address', TextareaType::class,[
                'attr' => [
                    'placeholder' => 'Adresse postale*',
                    'class' => 'form-control'],
                'label' => false,
            ])
            ->add('quartier', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Quartier*',
                    'class' => 'form-control'],
                'label' => 'Quartier',
            ])
            ->add('city', EntityType::class,[
                'class' => City::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy(' c.label', 'ASC');
                },
                'placeholder' => 'Ville*',
                'attr' => ['class' => 'form-control'],
                'label' => false,
                'choice_label' => 'label',
                'choice_translation_domain' => true
            ])
            ->add('categories', EntityType::class,[
                // looks for choices from this entity
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.label', 'ASC');
                },
                'placeholder' => 'Catégorie*',
                'attr' => ['class' => 'align-middle selectpicker form-control', 'title' => '*Catégories'],
                'label' => 'false',
                'choice_label' => 'label',
                'multiple' => true,
                'choice_translation_domain' => true
            ])
            ->add('photoFile1', FileType::class, [
                'label' => 'Photo N°1',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile2', FileType::class, [
                'label' => 'Photo N°2',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile3', FileType::class, [
                'label' => 'Photo N°3',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile4', FileType::class, [
                'label' => 'Photo N°4',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('submit', SubmitType::class,[
                'attr' => ['class' => 'btn btn-primary  py-2 px-4 w-50 mt-3'],
                'label' => 'Mettre à jour'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArtisanUser::class,
            'method' => 'post'
        ]);
    }
}
