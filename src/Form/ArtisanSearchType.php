<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Category;
use App\Entity\ArtisanSearch;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtisanSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', EntityType::class,[
                'class' => City::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy(' c.label', 'ASC');
                },
                'placeholder' => 'form.searchartisan.city',
                'attr' => ['class' => 'form-control'],
                'label' => false,
                'choice_label' => 'label',
                'choice_translation_domain' => true
            ])
            ->add('category', EntityType::class,[
                // looks for choices from this entity
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.label', 'ASC');
                },
                'placeholder' => 'form.searchartisan.category',
                'attr' => ['class' => 'form-control'],
                'label' => false,
                'choice_label' => 'label',
                'choice_translation_domain' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArtisanSearch::class,
            'method' => 'get',
        ]);
    }
}
