<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Nom complet',
                    'class' => 'form-control'],
                'label' => 'Nom',
            ])
            ->add('email', EmailType::class,[              
                'attr' => [
                    'placeholder' => 'Adresse e-mail',
                    'class' => 'form-control'],
                'label' => 'Email',
            ])
            ->add('subject', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Sujet',
                    'class' => 'form-control'],
                'label' => 'Sujet',
            ])
            ->add('message', TextareaType::class,[              
                'attr' => [
                    'placeholder' => 'Message',
                    'cols' => '30',
                    'row' => '7',
                    'class' => 'form-control'],
                'label' => 'Message',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
