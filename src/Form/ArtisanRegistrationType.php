<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Category;
use App\Entity\ArtisanUser;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArtisanRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Prénom*',
                    'class' => 'form-control'],
                'label' => 'Prénom',
            ])
            ->add('lastName', TextType::class,[                
                'attr' => [
                    'placeholder' => 'Nom*',
                    'class' => 'form-control'],
                'label' => 'Nom',
            ])
            ->add('heureFin', ChoiceType::class, [
                'mapped' => false,
                'attr' => ['class' => 'w-25 ml-3'],
                'choices'  => [
                    '00' => '00',
                    '01' => '01',
                    '02' => '02',
                    '03' => '03',
                    '04' => '04',
                    '05' => '05',
                    '06' => '06',
                    '07' => '07',
                    '08' => '08',
                    '09' => '09',
                    '10' => '10',
                    '11' => '11',
                    '12' => '12',
                    '13' => '13',
                    '14' => '14',
                    '15' => '15',
                    '16' => '16',
                    '17' => '17',
                    '18' => '18',
                    '19' => '19',
                    '20' => '20',
                    '21' => '21',
                    '22' => '22',
                    '23' => '23',
                ],
            ])
            ->add('minuteFin', ChoiceType::class, [
                'mapped' => false,
                'attr' => ['class' => 'w-25 ml-3'],
                'choices'  => [
                    '00' => '00',
                    '15' => '15',
                    '30' => '15',
                    '45' => '45',
                ],
            ])
            ->add('heureDebut', ChoiceType::class, [
                'mapped' => false,
                'attr' => ['class' => 'w-25 ml-4'],
                'choices'  => [
                    '00' => '00',
                    '01' => '01',
                    '02' => '02',
                    '03' => '03',
                    '04' => '04',
                    '05' => '05',
                    '06' => '06',
                    '07' => '07',
                    '08' => '08',
                    '09' => '09',
                    '10' => '10',
                    '11' => '11',
                    '12' => '12',
                    '13' => '13',
                    '14' => '14',
                    '15' => '15',
                    '16' => '16',
                    '17' => '17',
                    '18' => '18',
                    '19' => '19',
                    '20' => '20',
                    '21' => '21',
                    '22' => '22',
                    '23' => '23',
                ],
            ])
            ->add('minuteDebut', ChoiceType::class, [
                'mapped' => false,
                'attr' => ['class' => 'w-25 ml-3'],
                'choices'  => [
                    '00' => '00',
                    '15' => '15',
                    '30' => '15',
                    '45' => '45',
                ],
            ])
            ->add('sexe', ChoiceType::class, [
                'choices'  => [
                    'Homme' => 0,
                    'Femme' => 1,
                ],
            ])
            ->add('isEntreprise', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Etes vous une entreprise ?',
                'label' => true,
                'required' => false,
            ])
            ->add('photoFile', FileType::class, [
                'label' => 'Photo de profil',
                'attr' => ['class' => 'form-control',
                'placeholder' => 'Photo de profil'],
                'mapped' => false,
                'required' => false,
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'placeholder' => 'Décrivez votre activité',
                    'class' => 'form-control'],
                'label' => 'Description',
            ])
            ->add('hourlyCost', NumberType::class,[
                'attr' => [
                    'placeholder' => 'Coût horaire',
                    'class' => 'form-control tinymce',],
                'label' => 'Coût horaire',
            ])
            ->add('email', EmailType::class,[
                'attr' => [
                    'placeholder' => 'Adresse e-mail*',
                    'class' => 'form-control'],
                'label' => 'E-mail',
            ])
            ->add('password', PasswordType::class,[
                'attr' => [
                    'placeholder' => 'Mot de passe',
                    'class' => 'form-control'],
                'label' => true,
            ])
            ->add('confirmPassword', PasswordType::class,[
                'attr' => [
                    'placeholder' => 'Confirmation du mot de passe',
                    'class' => 'form-control'],
                'label' => false,
                
            ])
            ->add('phoneNumber', TelType::class,[
                'attr' => [
                    'placeholder' => 'Numéro de téléphone*',
                    'class' => 'form-control'],
                'label' => false,
            ])
            ->add('address', TextareaType::class,[
                'attr' => [
                    'placeholder' => 'Adresse postale*',
                    'class' => 'form-control'],
                'label' => false,
            ])
            ->add('quartier', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Quartier*',
                    'class' => 'form-control'],
                'label' => 'Quartier',
            ])
            ->add('city', EntityType::class,[
                'class' => City::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy(' c.label', 'ASC');
                },
                'placeholder' => 'Ville*',
                'attr' => ['class' => 'form-control'],
                'label' => false,
                'choice_label' => 'label',
                'choice_translation_domain' => true
            ])
            ->add('photoFile1', FileType::class, [
                'label' => 'Photo N°1',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile2', FileType::class, [
                'label' => 'Photo N°2',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile3', FileType::class, [
                'label' => 'Photo N°3',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile4', FileType::class, [
                'label' => 'Photo N°4',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('categories', EntityType::class,[
                // looks for choices from this entity
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.label', 'ASC');
                },
                'placeholder' => 'Catégorie*',
                'attr' => ['class' => 'align-middle selectpicker form-control', 'title' => 'Catégories*'],
                'label' => 'false',
                'choice_label' => 'label',
                'multiple' => true,
                'choice_translation_domain' => true
            ])
            ->add('submit', SubmitType::class,[
                'attr' => ['class' => 'btn btn-primary  py-2 px-4 w-50 mt-3'],
                'label' => 'S\'inscrire'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArtisanUser::class,
            'method' => 'post'
        ]);
    }
}
