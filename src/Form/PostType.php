<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Post;
use App\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,[                
                'attr' => [
                    'placeholder' => 'Titre',
                    'class' => 'form-control'],
                'label' => 'Nom',
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'placeholder' => 'Décrivez vos travaux',
                    'class' => 'form-control'],
                'label' => 'Description',
            ])
            ->add('email', EmailType::class,[
                'attr' => [
                    'placeholder' => 'Adresse e-mail*',
                    'class' => 'form-control'],
                'label' => 'E-mail',
            ])
            ->add('photoFile1', FileType::class, [
                'label' => 'Photo N°1',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile2', FileType::class, [
                'label' => 'Photo N°2',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile3', FileType::class, [
                'label' => 'Photo N°3',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('photoFile4', FileType::class, [
                'label' => 'Photo N°4',
                'attr' => [
                    'class' => 'ml-3 mr-3 w-75 form-control'],
                'mapped' => false,
                'required' => false,
                'label' => false,
            ])
            ->add('phone', TelType::class,[
                'attr' => [
                    'placeholder' => 'Numéro de téléphone*',
                    'class' => 'form-control'],
                'label' => false,
            ])
            ->add('firstname', TextType::class,[              
                'attr' => [
                    'placeholder' => 'Prénom*',
                    'class' => 'form-control'],
                'label' => 'Prénom',
            ])
            ->add('lastname', TextType::class,[                
                'attr' => [
                    'placeholder' => 'Nom*',
                    'class' => 'form-control'],
                'label' => 'Nom',
            ])
            ->add('category', EntityType::class,[
                // looks for choices from this entity
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.label', 'ASC');
                },
                'placeholder' => 'Catégories*',
                'attr' => ['class' => 'form-control'],
                'label' => false,
                'choice_label' => 'label',
                'choice_translation_domain' => true
            ])
            ->add('city', EntityType::class,[
                'class' => City::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy(' c.label', 'ASC');
                },
                'placeholder' => 'Ville*',
                'attr' => ['class' => 'form-control'],
                'label' => false,
                'choice_label' => 'label',
                'choice_translation_domain' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            'method' => 'post'
        ]);
    }
}
