<?php

namespace App\Entity;

use JsonSerializable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdOn;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ArtisanUser", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $about;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commentator;

    /**
     * @ORM\Column(type="integer")
     */
    private $likes;

    /**
     * @ORM\Column(type="integer")
     */
    private $dislikes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comment", inversedBy="comments", cascade={"persist"})
     */
    private $replyTo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="replyTo")
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->createdOn;
    }

    public function setCreatedOn(\DateTimeInterface $createdOn): self
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    public function getAbout(): ?ArtisanUser
    {
        return $this->about;
    }

    public function setAbout(?ArtisanUser $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getCommentator(): ?string
    {
        return $this->commentator;
    }

    public function setCommentator(string $commentator): self
    {
        $this->commentator = $commentator;

        return $this;
    }

    public function getLikes(): ?int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): self
    {
        $this->likes = $likes;

        return $this;
    }

    public function getDislikes(): ?int
    {
        return $this->dislikes;
    }

    public function setDislikes(int $dislikes): self
    {
        $this->dislikes = $dislikes;

        return $this;
    }

    public function getReplyTo(): ?self
    {
        return $this->replyTo;
    }

    public function setReplyTo(?self $replyTo): self
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(self $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setReplyTo($this);
        }
        return $this;
    }

    public function removeComment(self $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getReplyTo() === $this) {
                $comment->setReplyTo(null);
            }
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'commentator' => $this->commentator,
            'text'=> $this->text,
            'id'=> $this->id,
            'likes'=> $this->likes,
            'dislikes'=> $this->dislikes,
            'createdOn'=> $this->createdOn,
        );
    }
}
