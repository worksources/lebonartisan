<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ArtisanUser", mappedBy="category")
     */
    private $artisanUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="category", orphanRemoval=true)
     */
    private $posts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ArtisanUser", mappedBy="categories")
     */
    private $artisanUsersList;

    public function __construct()
    {
        $this->artisanUsers = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->artisanUsersList = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|ArtisanUser[]
     */
    public function getArtisanUsers(): Collection
    {
        return $this->artisanUsers;
    }

    public function addArtisanUser(ArtisanUser $artisanUser): self
    {
        if (!$this->artisanUsers->contains($artisanUser)) {
            $this->artisanUsers[] = $artisanUser;
            $artisanUser->setCategory($this);
        }

        return $this;
    }

    public function removeArtisanUser(ArtisanUser $artisanUser): self
    {
        if ($this->artisanUsers->contains($artisanUser)) {
            $this->artisanUsers->removeElement($artisanUser);
            // set the owning side to null (unless already changed)
            if ($artisanUser->getCategory() === $this) {
                $artisanUser->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setCategory($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getCategory() === $this) {
                $post->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArtisanUser[]
     */
    public function getArtisanUsersList(): Collection
    {
        return $this->artisanUsersList;
    }

    public function addArtisanUsersList(ArtisanUser $artisanUsersList): self
    {
        if (!$this->artisanUsersList->contains($artisanUsersList)) {
            $this->artisanUsersList[] = $artisanUsersList;
            $artisanUsersList->addCategory($this);
        }

        return $this;
    }

    public function removeArtisanUsersList(ArtisanUser $artisanUsersList): self
    {
        if ($this->artisanUsersList->contains($artisanUsersList)) {
            $this->artisanUsersList->removeElement($artisanUsersList);
            $artisanUsersList->removeCategory($this);
        }

        return $this;
    }
}
