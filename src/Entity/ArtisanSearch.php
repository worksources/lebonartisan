<?php

namespace App\Entity;

use App\Entity\City;
use App\Entity\Category;


/**
 */
class ArtisanSearch
{
    /**
     * @var Category
     */
    private $category;

    /**
     * @var City
     */
    private $city;

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }
}

?>